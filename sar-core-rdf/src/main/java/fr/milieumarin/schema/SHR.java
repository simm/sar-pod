/*
 * #%L
 * SUMARiS
 * %%
 * Copyright (C) 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.milieumarin.schema;

import net.sumaris.core.model.ModelVocabularies;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * Transcribing
 *
 * see http://id.milieumarinfrance.fr/schema/tscb
 */
public class SHR {
    public static final String PREFIX = ModelVocabularies.COMMON;
    public static final String NS = "http://id.milieumarinfrance.fr/schema/" + PREFIX + "/";

    public static String getURI() {
        return NS;
    }

    protected final static Resource resource(String local) {
        return ResourceFactory.createResource(NS + local);
    }

    protected final static Property property(String local) {
        return ResourceFactory.createProperty(NS + local);
    }

    public static final Resource NAMESPACE = resource(NS);

    public static final Resource ValidityStatus = resource("ValidityStatus");
    public static final Resource Status = resource("Status");
}
