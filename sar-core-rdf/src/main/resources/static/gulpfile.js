'use strict';

const gulp = require('gulp'),
  webserver = require('gulp-webserver');


function appServe() {
  // Serve, using light webserver
  return gulp.src('./')
    .pipe(webserver({
      path: '/',
      port: 4201,
      livereload: true,
      directoryListing: true,
      open: false,
      proxies: ['api', 'sparql', 'schema', 'data', 'webvowl']
          .map(path => ({
            source: '/' + path,
            target: 'http://localhost:8080/' + path
          }))
    }));
}

exports.serve = appServe;

exports.default = appServe;
