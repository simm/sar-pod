/*-
 * #%L
 * SAR :: RDF features
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var translations = {
    NEW_DATA_LINK: "Si besoin, vous pouvez <a href='https://mdm.sandre.eaufrance.fr/node/add/interlocuteur?origine=SIMM' target='_system'>faire une demande d'ajout</a> auprès du SANDRE (nécessite un compte)",
    RESULT_COUNT: "{{count}} résultats",
    PLUGIN: {
        ALL: "Tout",
        TAB_NAME: "Liste",
        PAGE_SIZE_DOTS: "Nombre de lignes :",
        YES: "Oui",
        NO: "Non",
        CODE: "Code",
        NAME: "Nom",
        ADDRESS: "Adresse",
        COUNTRY: "Pays",
        STATUS: "Etat",
        VALIDITY_STATUS: "Statut",
        DATES: "Date création / MAJ",
        UPDATE_DATE: "Date dernière modification",
        CREATION_DATE: "Date de création",
        IS_MAIN: "Etabl. siège ?",
        NO_DATA: "Aucun resultat.",
        DOWNLOAD: "Télécharger",
        BASE_FILE_NAME: "Interlocuteurs",
        SIRET_DOTS: 'Siret :',
        EDMO_DOTS: 'EDMO :',
        HELP: "Voir l'aide en ligne",
        HELP_URL: "https://gitlab.ifremer.fr/simm/sar-doc/-/blob/master/user-manual/search/organization.md"
    },
    TABLE: {
        // Columns
        LABEL: "Nom",
        SOURCE_URI: "Code",
        ADDRESS: "Adresse",
        COUNTRY: "Pays",
        VALIDITY_STATUS: "Statut",
        MODIFIED: "Date dernière modif.",
        CREATED: "Date de création",
        IS_MAIN: "Etabl. siège ?",
        SIRET: 'Siret',
        EDMO: 'EDMO',

        // Button / Labels
        PAGE_SIZE_DOTS: "Nombre de lignes : ",
        FILTER_PLACEHOLDER: "Filrer les résulats",
    }
}

function i18n(key, params) {
    var keys = key.split('.');
    var value = translations;

    for (var i = 0; i<keys.length; i++) {
        value = value[keys[i]];
        if (!value) return key;
    }
    // Not found: return the key
    if (!(typeof value === 'string')) return key;

    // Replace params
    if (params) {
        Object.keys(params).forEach(paramKey => {
            const paramValue = params[paramKey];
            value = value.replaceAll('{{' + paramKey + '}}', paramValue);
        })
    }

    return value;
}
