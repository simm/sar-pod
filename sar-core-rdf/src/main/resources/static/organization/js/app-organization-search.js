/*-
 * #%L
 * SUMARiS:: RDF features
 * %%
 * Copyright (C) 2018 - 2020 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


function AppOrganizationSearch(config) {
    const defaultConfig = {
        ids: {
            yasqe: 'yasqe',
            yasr: 'yasr',
            tabs: 'tabs',
            options: 'options',
            details: 'details'
        },
        onUriClick: undefined,
        onUriClickTarget: undefined,
        noResultMessage: 'PLUGIN.NO_DATA',
        exactMatch: false,
        limit: 50,
        prefix: 'this'
    };

    const endpointsById = {
        THIS: 'this',
        EAU_FRANCE: 'http://id.eaufrance.fr/sparql'
    };

    const INC_CODE_REGEXP = new RegExp(/^INC[0-9]+$/);
    const DATE_REGEXP = new RegExp(/^[12][0-9]{3}-[0-9]{2}-[0-9]{2}$/);

    const helper = new RdfHelper();
    const utils = new AppUtils();
    const prefixDefs = [
        {
            name: 'this',
            prefix: 'this',
            namespace: undefined
        }].concat(helper.constants.prefixes);


    const defaultQuery = "SELECT DISTINCT * WHERE {\n" +
        "  {{where}} \n" +
        "} LIMIT {{limit}}";

    const filtersMap = {
        // Schema filter
        rdfType: 'rdf:type {{rdfType}}',
        rdfsLabel: '?sourceUriUri rdfs:label ?label .',
        dwcScientificName: '?sourceUriUri dwc:scientificName ?scientificName .',

        // Regex filter, on a property
        exactMatch: '?{{searchField}}="{{q}}"',
        prefixMatch: 'regex( ?{{searchField}}, "^{{q}}", "i" )',
        anyMatch: 'regex( ?{{searchField}}, "{{q}}", "i" )',

        // Regex filter, on uri (code)
        codeExactMatch: 'strEnds( str(?sourceUri), "/{{code}}" )',
        codePrefixMatch: 'regex( str(?sourceUri), "/{{code}}$", "i" )',

        // Sandre filter
        edmoExactMatch: '?edmo="{{edmo}}"',
        edmoPrefixMatch: 'regex( ?edmo, "^{{edmo}}", "i" )',
        siretExactMatch: '?siret="{{siret}}"',
        siretPrefixMatch: 'regex( ?siret, "^{{siret}}", "i" )',
        validityStatusExactMatch: '?validityStatus = validityStatus:{{validityStatus}}',
        countryExactMatch: '?country=UCASE({{country}})',
        countryPrefixMatch: 'regex( ?country, "^{{country}}", "i" )',
    };
    const queries = Object.freeze([
        {
            id: 'local-name',
            name: 'Recherche SAR',
            canHandleTerm: (term) => term && term.trim().match(/^[A-Za-z ]+$/),
            yasrPlugin : 'organization',
            prefixes: ['dc', 'dcterms', 'rdf', 'rdfs', 'owl', 'skos', 'foaf', 'org', 's', 'inc', 'incdata', 'validityStatus', 'sirene',
                'sarshr', 'sartscb'],
            query: 'SELECT DISTINCT \n' +
                '  ?label ?sourceUri ?address ?country \n' +
                '  ?validityStatus ?created ?modified ?isMain ?siret ?edmo \n' +
                'WHERE {\n' +
                '  ?sourceUri rdfs:label ?label ;\n' +
                '       a org:Organization ;\n' +
                '       dc:identifier ?code .\n' +
                '  FILTER ( {{filter}} ) .\n' +
                '  OPTIONAL {\n' +
                '   ?sourceUri sarshr:transcribingItem _:edmo .\n' +
                '    _:edmo a sartscb:TranscribingItem ;\n' +
                //'      tscbItem:type ?edmoTypeUri ;\n' +
                '      rdfs:label ?edmo .\n' +
                '  }\n' +
                '  OPTIONAL {\n' +
                '    ?sourceUri sarshr:validityStatus ?validityStatus .\n' +
                '  }\n' +
                '  OPTIONAL {\n' +
                '    ?sourceUri org:hasprimarySite ?bnSite .\n' +
                '    OPTIONAL{ ?bnSite sirene:siret ?siret }\n' +
                '    OPTIONAL {\n' +
                '      ?sourceUri org:hasRegisteredSite ?bnSite\n' +
                '      BIND("1" as ?isMain)\n' +
                '    }\n' +
                '    ?bnSite org:siteAddress [\n' +
                '          s:streetAddress ?streetAddress ;\n' +
                '          s:postalCode ?postalCode ;\n' +
                '          s:addressLocality ?city ;\n' +
                '          s:addressCountry ?country\n' +
                '      ]\n' +
                '    BIND(\n' +
                '      REPLACE(REPLACE(\n' +
                '          CONCAT(?postalCode, \'|\', ?city),\n' +
                '          \'^[|]+\', \'\', \'i\'),\n' +
                '        \'[|]+\', \' \', \'i\')\n' +
                '      as ?postalCodeAndCity\n' +
                '    )  \n' +
                '    BIND(\n' +
                '      REPLACE(REPLACE(\n' +
                '          CONCAT(?streetAddress, \'|\', ?postalCodeAndCity),\n' +
                '          \'^[|]+\', \'\', \'i\'),\n' +
                '        \'[|]+\', \' \', \'i\')\n' +
                '      as ?address\n' +
                '    )\n' +
                '  }\n' +
                '  OPTIONAL {\n' +
                '    ?sourceUri dc:created|dcterms:created ?created ;\n' +
                '      dc:modified|dcterms:modified ?modified .\n' +
                '  }\n' +
                '}\n' +
              ' ORDER BY ASC (?label)\n' +
              ' LIMIT {{limit}}',
            filters: ['anyMatch'],
            binding: {
                searchField: 'label'
            }
        },
        {
            id: 'remote-name',
            name: 'Recherche SANDRE',
            canHandleTerm: (term) => term && term.trim().match(/^[A-Za-z ]+$/),
            yasrPlugin : 'organization',
            debug: false,
            prefixes: ['dc', 'dcterms', 'rdf', 'rdfs', 'owl', 'skos', 'foaf', 'org', 'gr', 's', 'inc', 'incdata', 'com', 'validityStatus', 'sirene'],
            query: 'SELECT DISTINCT \n' +
              '  ?label ?sourceUri ?address ?country \n' +
              '  ?validityStatus ?created ?modified ?isMain ?siret ?edmo \n' +
              'WHERE {\n' +

              // -- Sandre endpoint part
              ' { SERVICE <{{eauFranceEndpoint}}> {\n' +
              '  ?sourceUri a inc:Interlocuteur ;\n' +
              '    rdfs:label ?label ;\n' +
              '    inc:CdInterlocuteur ?code ;\n' +
              '    inc:StInterlocuteur ?validityStatus ;\n' +
              '    inc:CdAlternatifInt _:simm .\n' +
              '  _:simm inc:OrCdAlternInterlocuteur "SIMM" .\n' +
              '  FILTER({{filter}})\n' +
              '  OPTIONAL {\n' +
              '    ?sourceUri inc:CdAlternatifInt _:edmo .\n' +
              '    _:edmo inc:OrCdAlternInterlocuteur "EDMO" ;\n' +
              '      inc:CdAlternInterlocuteur ?edmo .\n' +
              '  }\n' +
              '  OPTIONAL {\n' +
              '        ?sourceUri inc:Etablissement ?bnEtab .\n' +
              '        OPTIONAL { ?bnEtab sirene:siret ?siret . }\n' +
              '        OPTIONAL { ?bnEtab inc:EtabSiege ?isMain . }\n' +
              '  }\n' +
              '  OPTIONAL {\n' +
              '    ?sourceUri inc:AdresseInterlocuteur ?bnAddress .\n' +
              '    OPTIONAL { ?bnAddress inc:Compl2Adresse ?addressCompl2 . }\n' +
              '    OPTIONAL { ?bnAddress inc:Compl3Adresse ?addressCompl3 . }\n' +
              '    OPTIONAL { ?bnAddress inc:NumLbVoieAdresse ?addressRoad . }\n' +
              '    OPTIONAL { ?bnAddress inc:LgAcheAdresse ?postalCodeAndCity . }\n' +
              '    BIND(\n' +
              '      REPLACE(REPLACE(\n' +
              '        CONCAT(?addressCompl2, \'|\', ?addressCompl3, \'|\', ?addressRoad),\n' +
              '          \'^[|]+\', \'\', \'i\'),\n' +
              '        \'[|]+\', \', \', \'i\')\n' +
              '      as ?streetAddress\n' +
              '    )\n' +
              '    BIND(\n' +
              '      REPLACE(REPLACE(\n' +
              '        CONCAT(?streetAddress, \'|\', ?postalCodeAndCity),\n' +
              '          \'^[|]+\', \'\', \'i\'),\n' +
              '        \'[|]+\', \', \', \'i\')\n' +
              '      as ?address\n' +
              '    )\n' +
              '  }\n' +
              '  OPTIONAL {\n' +
              '    ?sourceUri inc:PaysInterlocuteur|inc:Pays _:country .\n' +
              '    _:country com:NomPays ?country .\n' +
              '  }\n' +
              '  OPTIONAL {\n' +
              '    ?sourceUri dc:created|inc:DateCreInterlocuteur ?created ;\n' +
              '      dc:modified|inc:DateMAJInterlocuteur ?modified .\n' +
              '  }\n' +
              '  OPTIONAL { ?sourceUri skos:exactMatch|owl:sameAs ?exactMatch }\n' +
              ' }}\n' +
              '}\n' +
              ' ORDER BY ASC (?label)\n' +
              ' LIMIT {{limit}}',
            filters: ['anyMatch'],
            binding: {
                eauFranceEndpoint: endpointsById.EAU_FRANCE,
                searchField: 'label'
            }
        },
     ]);

    // SparQL var
    let defaultEndpoint,
        endpoints;

    // Form and app variables
    let output,
        inputSearch,
        details,
        selectedQueryIndex = -1,
        totalRowCount = -1,
        ready = false,
        debug = false;

    // YasGui
    let yasqe, yasr, onYasqeQueryResponse;

    /* -- Log and message -- */

    function onError(evt)
    {
        log('ERROR: ' + evt.data, 'text-error');
    }


    function log(message, classAttribute)
    {
        const pre = document.createElement("p");
        if (classAttribute) {
            const classes = classAttribute.split(" ");
            for (let i=0; i< classes.length; i++) {
                pre.classList.add(classes[i]);
            }
        }
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);

        output.classList.remove('d-none');

    }

    function clearLog() {
        output.innerHTML = "";
    }

    /* -- Init functions -- */

    function init(event, opts) {
        opts = opts || {};

        if (opts.skipPrefixLoad !== true && window.location && window.location.origin) {

            // Compute default endpoint
            defaultEndpoint = window.location.origin + '/sparql';

            endpointsById.THIS = defaultEndpoint;

            // Update the default prefix
            helper.loadDefaultPrefix((prefixDef) => {
                prefixDefs[0] = {
                    ...prefixDefs[0],
                    namespace: prefixDef.namespace,
                    prefix: prefixDef.prefix
                };
                prefixDefs.push({
                    name: 'sarshr',
                    prefix: 'sarshr',
                    namespace: prefixDef.namespace + 'shr/' + prefixDef.version + '/',
                });
                prefixDefs.push({
                    name: 'sartscb',
                    prefix: 'sartscb',
                    namespace: prefixDef.namespace + 'tscb/' + prefixDef.version + '/',
                });
                prefixDefs.push({
                    name: 'sarorg',
                    prefix: 'sarorg',
                    namespace: prefixDef.namespace + 'org/' + prefixDef.version + '/',
                });

                console.debug('[organization] All URI prefixes loaded: ', prefixDefs);
                init(event, {skipPrefixLoad: true}); // Loop to finish init
            });
            return;
        }

        console.debug("[organization] Init organization search app...");
        config = {
            ...defaultConfig,
            ...config
        };

        // Mount some HTML elements
        inputSearch = document.getElementById("q");
        output = document.getElementById("output");

        // Add press enter listeners, to all search fields
        $("input[type='text']")
          .filter((i, input) => $(input).hasClass("form-control"))
          .each((i, input) => addPressEnterListener(input));

        // Help link
        $('.help-link')
            .attr('title', i18n("PLUGIN.HELP"))
            .attr('href', i18n("PLUGIN.HELP_URL"))
            .removeClass('d-none');

        // Add tooltips
        initTooltips();

        initAutocomplete();

        // Hide toast for missing element
        $('#create-missing-toast').addClass('d-none');

        // Collect endpoints, from default, endpoint map, and queries
        endpoints = Object.keys(endpointsById).map(key => endpointsById[key])
            .reduce((res, ep) => (!ep || res.findIndex(endpoint => endpoint === ep) !== -1) ? res : res.concat(ep),
                [defaultEndpoint]);

        details = document.getElementById(config.ids.details);

        // Add tabs
        drawTabs();

        ready = true;

        // Restore previous query
        const hasSomeParams = restoreQueryParam();
        if (hasSomeParams) {
            doSearch();
        }
    }

    function restoreQueryParam() {

        const params = parseLocationHash();
        if (!params) return false;

        inputSearch.value = params.q || "";
        let hasSomeParams = inputSearch.value.trim().length > 0 || false;

        if (params.code || params.edmo || params.siret || params.validityStatus || params.country ) {
            hasSomeParams = hasSomeParams || true
            utils.setInputValue('code', params.code || '');
            utils.setInputValue('edmo', params.edmo || '');
            utils.setInputValue('siret', params.siret || '');
            utils.setInputValue('validityStatus', params.validityStatus || 'Validé');
            utils.setInputValue('country', params.country || '');
        }

        if (params.debug) {
            showDebug(true, {emitEvent: false});

            if (params.query && params.query.trim().length) {
                initYase();
                yasqe.setValue(params.query.trim());
            }
        }

        return hasSomeParams;
    }

    /**
     * Update the config (partially)
     * @param newConfig
     * @param opts
     */
    function setConfig(newConfig, opts) {
        opts = opts || {};
        const oldConfigStr = JSON.stringify(config);
        config = {
            ...config,
            ...newConfig
        };
        const changed = JSON.stringify(config) !== oldConfigStr;

        // Something changed
        if (changed) {

            // re-run the search (if NOT silent moe)
            if ((!opts || opts.silent !== true) && selectedQueryIndex !== -1) {
                if (inputSearch.value) {
                    doSearch(inputSearch.value);
                }

                // Or update the query
                else {
                    const query = queries[selectedQueryIndex];
                    doUpdateQuery(query.q);
                }
            }
        }
    }

    /* -- Queries (as tabs) -- */

    function drawTabs(elementId) {

        elementId = elementId || config.ids.tabs;
        config.ids.tabs = elementId;

        // Queries
        const tabs = queries.map((example, index) => {
            if (!example.id) return ''; // Skip if no name (e.g. default)
            const debugClassList = example.debug ? ['debug', 'd-none'].join(' ') : '';
            const hash = window.location.hash || '#';
            return '<li class="nav-item "'+ debugClassList + '">' +
                '<a href="'+hash+'" class="nav-link '+ example.id + ' ' + debugClassList +'"' +
                ' onclick="app.selectQuery('+index+')">'+example.name+'</a></li>';
        }).join('\n');

        const innerHTML = tabs && "<ul class=\"nav nav-tabs\">" + tabs + "</ul>" || "";

        const element = document.getElementById(elementId);
        element.innerHTML = innerHTML;

    }

    function selectQuery(index, opts) {
        opts = opts || {};
        if (selectedQueryIndex === index) return; // Skip if same

        console.info('[organization] Change tab index to: ' + index, opts);
        const query = queries[index];
        if (query) {
            // Remember this choice
            selectedQueryIndex = index;
            if (query.q) {
                inputSearch.placeholder = query.q;
            }

            // re-run the search (if NOT silent moe)
            if (opts.silent !== true) {
                doSearch();
            }

            $('#' + config.ids.tabs + ' a').removeClass('active');
            $('#' + config.ids.tabs + ' a.' + query.id).toggleClass('active');

        }
    }

    function initYase(opts) {
        opts = opts || {};

        const requestConfig = {
            endpoint: defaultEndpoint || endpoints.length && endpoints[0] || undefined
            // headers: () => ({
            //     //'token': '' // TODO add authenticated token
            // })
        };

        if (!yasqe) {
            const element = document.getElementById(config.ids.yasqe);
            if (!element) throw new Error('Cannot find div with id=' + config.ids.yasqe);

            yasqe = new Yasqe(element, {
                requestConfig
            });

            // Listen query response
            yasqe.on("queryResponse", (yasqe, res, duration) => {
                if (onYasqeQueryResponse) return onYasqeQueryResponse(yasqe, res, duration)
            });
        }

        // Apply the correct query response function
        onYasqeQueryResponse = opts.queryResponse || displayResponse;
    }

    function initYasr() {
        // Remove old config (e.g. from Taxon search page)
        window.localStorage.removeItem('yasr__config');

        const prefixes = yasqe.getPrefixesFromQuery();
        if (!yasr) {

            const element = document.getElementById(config.ids.yasr);
            if (!element) throw new Error('Cannot find div with id=' + config.ids.yasr);

            Yasr.registerPlugin("organization", YasrOrganizationPlugin);

            YasrOrganizationPlugin.prototype.defaults.onUriClick = config.onUriClick;
            YasrOrganizationPlugin.prototype.defaults.onUriClickTarget = config.onUriClickTarget;
            YasrOrganizationPlugin.prototype.defaults.onPageSizeChanged = (pageSize) => {
                config.limit = pageSize;
                doSearch(undefined, {limit: pageSize});
            };
            YasrOrganizationPlugin.prototype.defaults.noResultMessage = config.noResultMessage;
            YasrOrganizationPlugin.prototype.defaults.pageSize = config.limit;

            const pluginOrder = isDebug()
              ? ["organization", "table", "response"]
              : ["organization", "table"];

            yasr = new Yasr(element, {
                pluginOrder,
                prefixes
            });

            const yasrPlugin = window.localStorage.getItem('yasr__plugin');
            if (yasrPlugin) yasr.selectPlugin(yasrPlugin);

            // Listen results changes (e.g when user switch to another Yasr plugin)
            $(yasr.resultsEl).on('DOMSubtreeModified', () => setTimeout(afterShowResults));
        }
        else {
            yasr.config.prefixes = prefixes;
        }

    }

    function showDebug(enable, opts) {
        if (enable) {
            $('.debug').removeClass('d-none');
        }
        else {
            $('.debug').addClass('d-none');
        }
        if (enable !== debug) debug = enable;

        // Update the location hash
        if (!opts || opts.emitEvent !== false) {
            updateLocationHash();
        }
    }

    function isDebug() {
        return debug === true;
    }

    /* -- Search -- */

    function doSubmit(event) {
        if (event) {
            if (event.defaultPrevented) return;
            event.stopPropagation();
            event.preventDefault();
        }
        doSearch();
        return false;
    }

    function doSearch(searchText, opts) {
        // Compute search options
        opts = computeSearchOptions(opts);

        searchText = searchText || inputSearch.value;

        hideResult();

        showLoading();

        try {
            log("SEARCH: " + searchText + ' ' + JSON.stringify(opts), "text-muted");

            // Compute the query
            doUpdateQuery(searchText, opts);

            runQuery();

            updateLocationHash(searchText, opts);
        }
        catch(error) {
            console.error(error);
            onError({data: (error && error.message || error)});
            hideLoading();
        }
    }

    function computeSearchOptions(opts) {
        opts = {
            filters: [],
            binding: {},
            ...config,
            ...opts
        };

        // INC code
        let code = (utils.getInputValue('code') || '').trim();
        // When no code, try to parse he input search, to find a code
        if (!code.length && inputSearch.value.trim().length > 0) {
            const codes = utils.getSearchTerms('q')
              .filter(s => INC_CODE_REGEXP.test(s));
            if (codes.length === 1) {
                code = codes[0];
                inputSearch.value = inputSearch.value.replace(code, '');
                utils.setInputValue('code', code);
            }
        }
        if (code.length) {
            opts.binding = {
                ...opts.binding,
                code
            };
            if (!opts.filters.includes('codeExactMatch')) opts.filters.push('codeExactMatch');
        }

        // EDMO code
        const edmo = (document.getElementById("edmo").value || '').trim();
        if (edmo.length) {
            opts.binding = {
                ...opts.binding,
                edmo
            };
            if (!opts.filters.includes('edmoExactMatch')) opts.filters.push('edmoExactMatch');
        }

        // Add SIRET filter
        const siret = (utils.getInputValue("siret") || '').trim();
        if (siret.length) {
            opts.binding = {
                ...opts.binding,
                siret
            };
            if (!opts.filters.includes('siretExactMatch')) opts.filters.push('siretExactMatch');
        }

        // Validity Status
        const validityStatus = (utils.getInputValue('validityStatus') || '').trim();
        if (validityStatus.length) {
            opts.binding = {
                ...opts.binding,
                validityStatus
            };
            if (!opts.filters.includes('validityStatusExactMatch')) opts.filters.push('validityStatusExactMatch');
        }

        // Country
        const country = (utils.getInputValue('country') || '').trim().toUpperCase();
        if (country.length) {
            opts.binding = {
                ...opts.binding,
                country
            };
            if (!opts.filters.includes('countryPrefixMatch')) opts.filters.push('countryPrefixMatch');
        }

        return opts;
    }

    function runQuery() {
        setTimeout(() => {
            yasqe.queryBtn.click();
        })
    }

    function updateLocationHash(searchText, opts) {
        if (!window.location) return;

        searchText = searchText || (inputSearch && inputSearch.value) || (document.getElementById("q").value);
        let hash = searchText && searchText.trim().length ? ("&q=" + searchText.trim()) : "&";

        opts = opts || computeSearchOptions(opts);

        if (opts && opts.binding.code) {
            hash += "&code=" + opts.binding.code;
        }
        if (opts && opts.binding.edmo) {
            hash += "&edmo=" + opts.binding.edmo;
        }
        if (opts && opts.binding.siret) {
            hash += "&siret=" + opts.binding.siret;
        }
        if (opts && opts.binding.validityStatus && opts.binding.validityStatus !== 'Validé') {
            hash += "&validityStatus=" + opts.binding.validityStatus;
        }
        if (opts && opts.binding.country) {
            hash += "&country=" + opts.binding.country;
        }

        if (config.hash) {
            hash += "&" + config.hash;
        }

        if (isDebug()) {
            hash += "&debug";
        }

        // Update location hash
        window.location.hash = hash.substr(1); // Remove first '&'
    }

    function parseLocationHash() {
        const result = {};
        if (!window.location || !window.location.hash || window.location.hash.length === 1) return;

        const hashParts = (window.location.hash || '#').substr(1).split('&');
        (hashParts || []).forEach(param => {
            const paramParts = param.split('=', 2);
            const paramName = paramParts[0];
            if (paramName.trim().length) {
                const paramValue = decodeURIComponent(paramParts[1] || true);
                result[paramName.trim().toLowerCase()] = paramValue;
            }
        });

        console.debug('[organization] Hash parameters: ', result);

        return result;
    }



    function doUpdateQuery(searchText, opts) {

        opts = opts || {};

        opts.queryIndex = opts.queryIndex >= 0 ? opts.queryIndex : selectedQueryIndex;

        searchText = searchText || inputSearch.value;

        let searchTerms;
        if (typeof searchText === "string") {
            searchTerms = utils.getSearchTerms('q');
        }
        else if (typeof searchText === "array") {
            searchTerms = searchText.map(s => s.trim()).filter(s => s.length > 0);
        }
        else {
            throw new Error("Invalid argument: " + searchText);
        }
        if (searchTerms.length > 1) {
            console.info("Multiple search:", searchTerms);
        }

        // Auto set exact match, if not set yet
        if (config.exactMatch === undefined) {
            config.exactMatch = (searchText.indexOf('*') === -1);
        }

        // Auto select example
        if (opts.queryIndex === -1) {
            opts.queryIndex = queries.map((example, index) => {
                const count = searchTerms.reduce((count, searchTerm) =>
                        ((example.canHandleTerm && example.canHandleTerm(searchTerm)) ? count + 1 : count)
                    , 0);
                return {index,count};
            })
                .sort((e1, e2) => e1.count === e2.count ? 0 : (e1.count > e2.count ? -1 : 1))
                // Take the example that match must of search terms
                .map(e => e.index)[0];
            console.info("[organization] Auto select tab index:", opts.queryIndex);
        }
        if (opts.queryIndex !== selectedQueryIndex) {
            selectQuery(opts.queryIndex, {silent: true});
        }

        const queryDef = queries[opts.queryIndex];
        opts = {
            limit: config.limit || 50,
            exactMatch: config.exactMatch,
            // Override from the selected query
            ...queryDef,
            // Override using given options
            ...opts,
            filters: (opts.filters||[]).reduce(
                (res, item) => res.includes(item) ? res : res.concat(item), // Remove duplication
                    queryDef.filters || [])
        };
        opts.q = undefined;
        if (!opts.filters.length) throw new Error('Missing query filter');

        try {
            initYase({
                queryResponse: opts.queryResponse
            });

            let binding = {
                ...queryDef.binding,
                ...opts.binding,
                limit: opts.limit
            };

            // Prepare query for replacements
            let nbLoop = 0;
            let queryString = (opts.query || defaultQuery);

            while (queryString.indexOf('{{') !== -1 && nbLoop < 10) {
                queryString = searchTerms.reduce((query, q) => {

                    // Create filter clause (include only if '{{q}})
                    const filterClause = concatFilter(opts.filters, {
                        selector: filter => filter.indexOf('{{q}}') !== -1,
                        operator: '&&'
                    });

                    // Insert filter inside the query
                    if (filterClause.length) {
                        query = query.replace('#~filter', '\n\t|| (' + filterClause + ') #~filter\n\t')
                          .replace('{{filter}}', '(' + filterClause + ' #~filter)\n\t');
                    }

                    // Replace wildcards by regexp, if NOT exact match
                    binding.q = opts.exactMatch ? q
                      : q.trim()
                        .replace(/[*]+/g, '.*')
                        // Remove accent characters
                        .replace(/[çàâôéèêùûÇÀÂÔÉÈÊÙÛ]/g, '.');
                    binding.defaultPrefix = prefixDefs[0].prefix;

                    // Bind params
                    return Object.keys(binding).reduce((query, key) => {
                        return query.replace('{{' + key + '}}', binding[key])
                    }, query);
                }, queryString);

                nbLoop++;
            }

            // Prepare query for replacements
            queryString = queryString.replace('#~filter)', ')\n\t&& {{filter}}');
            nbLoop = 0;

            while (queryString.indexOf('{{') !== -1 && nbLoop < 10) {
                // Create filter clause (include only if NOT '{{q}})
                const filterClause = concatFilter(opts.filters, {
                    selector: filter => filter.indexOf('{{q}}') === -1,
                    operator: '&&'
                });

                // Insert filter inside the query
                if (filterClause.length) {
                    // Insert into filter
                    queryString = queryString.replace('#~filter', '\n\t|| (' + filterClause + ') #~filter\n\t')
                        .replace('{{filter}}', '(' + filterClause + ') #~filter\n\t');
                }

                // Bind params
                queryString = Object.keys(binding).reduce((query, key) => {
                    return query.replace('{{' + key + '}}', binding[key])
                }, queryString);

                nbLoop++;
            }

            // Compute the query
            queryString = queryString.replace('&& {{filter}}', '')
                .replace('#~filter', '')
                .replace('{{filter}}', 'true');

            // Remove LIMIT -1 => no limit
            queryString = queryString.replace('LIMIT -1', '');


            yasqe.setValue(queryString);
            log("QUERY: " + queryString);

            // Add prefixes
            const prefixes = (opts.prefixes || [])
                .map(p => p === 'this' ? prefixDefs[0].prefix : p) // Replace 'this' by default prefix
                .reduce((res, prefix) => {
                const def = prefixDefs.find(def => def.prefix === prefix);
                res[prefix] = def && def.namespace || undefined;
                return res;
            }, {});
            yasqe.addPrefixes(prefixes);
        }
        catch(error) {
            console.error(error);
            onError({data: (error && error.message || error)});
        }
    }

    // Create filter clause
    function concatFilter(filters, opts) {
        const selector = opts && opts.selector || ((_) => true);
        const operator = opts && opts.operator || '&&';
        return (filters || [])
            .map(key => {
                // If exactMatch, replace 'prefixMatch' with 'exactMatch'
                if (opts.exactMatch) {
                    if (key === 'prefixMatch' || key === 'anyMatch') return 'exactMatch';
                    if (key === 'cityPrefixMatch' || key === 'cityAnyMatch') return 'cityExactMatch';
                    if (key === 'codePrefixMatch') return 'codeExactMatch';
                    if (key === 'edmoPrefixMatch') return 'edmoExactMatch';
                    if (key === 'siretPrefixMatch') return 'siretExactMatch';
                    if (key === 'countryPrefixMatch') return 'countryExactMatch';
                }
                return key
            })
            .map(key => filtersMap[key])
            .filter(selector)
            .join('\n\t' + operator + ' ');
    }

    function showLoading(enable) {
        // Show
        if (enable !== false) {
            $('#loading').removeClass('d-none');
        }
        // Hide
        else {
            $('#loading').addClass('d-none');
        }
    }

    function hideLoading() {
        showLoading(false);
    }

    function displayResponse(yasqe, response, duration) {
        log('RESPONSE: received in ' + duration + 'ms');

        initYasr();

        if (!yasr.selectedPlugin) {
            const yasrPlugin = queries[selectedQueryIndex] && queries[selectedQueryIndex].yasrPlugin;
            if (yasrPlugin) yasr.selectPlugin(yasrPlugin);
        }

        yasr.setResponse(response);

        // Count total rows
        helper.executeCountFromSelect(yasqe.getValue(), {
            success: (count) => {
                totalRowCount = count;
                hideLoading();
                showResult();
            }
        });
    }

    function showResult(enable) {
        // Show
        if (enable !== false) {


            if (isDebug()) {
                $('#' + config.ids.tabs).removeClass('d-none');
                showResultResponse();
            }
            else {
                hideResultResponse();
            }

            $('#' + config.ids.options + ' #exactMatch').prop("checked", config.exactMatch !== false);

            afterShowResults();

            // Show Yasr div
            $('#' + config.ids.yasr).removeClass('d-none');


            hideDetails();
        }
        // Hide
        else {
            $('#' + config.ids.yasr).addClass('d-none');

            if (yasr && yasr.dataElement) {
                yasr.dataElement.classList.remove('visible');
            }

            // Reset row count
            totalRowCount = -1;
        }
    }

    function hideResult() {
        showResult(false);
    }

    function afterShowResults() {

        const countMessage = totalRowCount !== -1 ? i18n('RESULT_COUNT', {count: totalRowCount}) : '';
        if (yasr.dataElement.innerText === countMessage) return; // Already patched: skip (avoid infinite loop)

        console.info("[organization] Updating Yasr results ...");
        const yasrId = '#'+ config.ids.yasr;

        // Update total row count
        yasr.dataElement.innerText = countMessage;
        yasr.dataElement.classList.add('visible');

        const yasrPlugin = yasr.selectedPlugin;

        if (yasr.selectedPlugin === 'table') {

            $(yasrId + ' .dataTable').addClass('table-striped');

            // Translate some labels
            $(yasrId + ' .pageSizerLabel').text(i18n("TABLE.PAGE_SIZE_DOTS"));
            $(yasrId + ' .tableSizer').addClass('form-select form-select-sm')
              .css('display', 'inline-block')
              .css('width', '30%');

            $(yasrId + ' .tableFilter')
              .attr('placeholder', i18n("TABLE.FILTER_PLACEHOLDER"))
              .addClass('form-control form-control-sm')
              .css('width', '40%');
            $(yasrId + ' .dataTables_empty').html(i18n('PLUGIN.NO_DATA')
              + "<br/><small>"
              + i18n('NEW_DATA_LINK')
              + "</small>");

            // Translate column headers
            $(yasrId + ' .dataTable thead th').each((i, th) => {
                const columnName = th.innerText;
                const i18nKey = columnName && ('TABLE.' + utils.changeCaseToUnderscore(columnName).toUpperCase()) || undefined;
                const i18nColumnName = i18nKey && i18n(i18nKey);
                if (i18nColumnName !== i18nKey) {
                    th.innerHTML = '<b>' + i18nColumnName + '</b>';
                }
            });

            // Remove URI prefix
            $(yasrId + ' .dataTable a.iri').each((i, a) => {
                const uriParts = a.innerText.split(':');
                if (uriParts.length > 1) {
                    a.innerText = uriParts.slice(1).join(':');
                }
            });

            // Format dates
            $(yasrId + ' .dataTable td span.nonIri')
              .filter((i, ele) => DATE_REGEXP.test(ele.innerText))
              .each((i, ele) => {
                const dateParts = ele.innerText.split('-');
                if (dateParts.length === 3) {
                    ele.innerText = dateParts.reverse().join('/');
                }
            });

            // Format boolean
            $(yasrId + ' .dataTable td span.nonIri')
              .filter((i, ele) => ele.innerText === '1')
              .each((i, ele) => {
                  ele.innerText = i18n("PLUGIN.YES");
              });

            // Help link
            $(yasrId + ' .yasr_external_ref_btn')
              .attr('title', i18n("PLUGIN.HELP"))
              .attr('href', i18n("PLUGIN.HELP_URL"));
        }

        // Controls button color
        $(yasrId + ' .yasr_downloadIcon')
          .css('color', 'var(--indigo)');
        $(yasrId + ' .yasr_external_ref_btn')
          .css('color', 'var(--bs-primary)');


        // Save the selected plugin
        window.localStorage.setItem('yasr__plugin', yasrPlugin);
    }

    function showDetails(url) {

        const detailsContainer = $('#' + config.ids.details);
        const iframe = $('#' + config.ids.details + " iframe");

        // Show
        if (url) {
            // Hide iframe
            iframe.addClass('d-none');

            // Show the details loading spinner
            detailsContainer.addClass('loading').removeClass('d-none');

            // Change the iframe src attribute
            iframe.attr('src', url);
            iframe.on('load', function() {
                console.debug("Iframe loaded !", arguments);

                // Hide loading spinner
                detailsContainer.removeClass('loading');

                // Show iframe
                iframe.removeClass('d-none');

            });
        }

        // Hide
        else {
            detailsContainer.addClass('d-none').removeClass('loading');

            // Hide iframe
            iframe.addClass('d-none');

            // Remove iframe content
            iframe.attr('src', undefined);
        }
    }

    function hideDetails() {
        showDetails(false);
    }

    function showResultResponse(enable) {
        // Show
        if (enable !== false) {
            $('div.yasr_btn.select_response').removeClass('d-none');
        }
        // Hide
        else {
            $('div.yasr_btn.select_response').addClass('d-none');
        }
    }

    function hideResultResponse() {
        showResultResponse(false);
    }

    function addPressEnterListener(ele) {
        ele.addEventListener("keydown", function(event) {
            event = event || window.event;
            if (event.keyCode == 13) {
                event.preventDefault();
                doSearch();
            }
        }, false);
    }

    function initTooltips() {
        $('[data-toggle="tooltip"]').tooltip({
            placement : 'bottom', html: true
        });
    }

    function initAutocomplete() {
        loadCountries(countries => {
            $('#country').autocomplete({
                source: countries
                  //.map(capitalize)
                  .map(country => ({
                      label: country,
                      value: country,
                  })),
                // minimum number of characters to trigger the autocomplete list
                minLength: 0,
                // auto set focus on the first matched item
                autoFocus: false
            });
        });
    }

    function loadCountries(callback) {
        helper.executeSparql('PREFIX s: <http://schema.org/>\n' +
                'PREFIX org: <http://www.w3.org/ns/org#>\n' +
                'SELECT DISTINCT \n' +
                '  ?country\n' +
                'WHERE {\n' +
                '  ?sourceUri a org:Organization ;\n' +
                '    org:hasprimarySite ?bnSite .\n' +
                '  ?bnSite org:siteAddress [\n' +
                '    s:addressCountry ?country\n' +
                '  ]\n' +
                '} LIMIT 50', {
            success: callback,
            error: (err) => {
                console.error('[organization] Cannot load countries: ', err);
            }
        });
    }

    function clearInputs(){
        $("input[type='text']")
          .filter((i, input) => $(input).hasClass("form-control"))
          .each((i, input) => input.value = '');
    }

    // Start
    $(document).ready(() => init());

    return {
        selectQuery,
        showDebug,
        setConfig,
        clearLog,
        clearInputs,
        drawTabs,
        doSubmit,
        doSearch,
        showDetails,
        hideDetails
    }
}
