package net.sumaris.rdf.core.model;

import net.sumaris.core.model.referential.structure.CoastalStructureType;
import net.sumaris.rdf.AbstractTest;
import net.sumaris.rdf.DatabaseResource;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

public class ModelURIsTest extends AbstractTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.readDb();

    @Test
    public void getDataUri() {
        String schemaUri = "http://id.milieumarinfrance.fr/schema/cst/0.1/";
        String dataUri = ModelURIs.getDataUri(schemaUri, CoastalStructureType.class);
        Assert.assertEquals("http://id.milieumarinfrance.fr/data/cst/CoastalStructureType", dataUri);
    }

    public static void main(String[] args) {
        ModelURIsTest test = new ModelURIsTest();
        test.getDataUri();
    }
}
