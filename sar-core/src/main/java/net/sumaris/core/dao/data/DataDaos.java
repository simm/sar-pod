package net.sumaris.core.dao.data;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 - 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sumaris.core.dao.technical.Daos;
import net.sumaris.core.model.administration.user.Department;
import net.sumaris.core.model.administration.user.Person;
import net.sumaris.core.model.data.IDataEntity;
import net.sumaris.core.model.data.IWithRecorderDepartmentEntity;
import net.sumaris.core.model.data.IWithRecorderPersonEntity;
import net.sumaris.core.util.Beans;
import net.sumaris.core.vo.administration.user.DepartmentVO;
import net.sumaris.core.vo.administration.user.PersonVO;
import net.sumaris.core.vo.data.IDataVO;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>*
 */
public class DataDaos extends Daos {

    protected DataDaos() {
        super();
        // helper class does not instantiate
    }

    public static <T extends Serializable> void copyDataProperties(EntityManager entityManager,
                                                                   IDataVO<T> source,
                                                                   IDataEntity<T> target,
                                                                   boolean copyIfNull,
                                                                   String... excludeProperty) {

        Beans.copyProperties(source, target, excludeProperty);

        // Recorder department
        copyRecorderDepartment(entityManager, source, target, copyIfNull);

    }

    public static <T extends Serializable> void copyRecorderDepartment(EntityManager entityManager,
                                                                       IWithRecorderDepartmentEntity<T, DepartmentVO> source,
                                                                       IWithRecorderDepartmentEntity<T, Department> target,
                                                                       boolean copyIfNull) {
        // Recorder department
        if (copyIfNull || source.getRecorderDepartment() != null) {
            if (source.getRecorderDepartment() == null || source.getRecorderDepartment().getId() == null) {
                target.setRecorderDepartment(null);
            } else {
                target.setRecorderDepartment(load(entityManager, Department.class, source.getRecorderDepartment().getId()));
            }
        }
    }

    public static <T extends Serializable> void copyRecorderPerson(EntityManager entityManager,
                                                                   IWithRecorderPersonEntity<T, PersonVO> source,
                                                                   IWithRecorderPersonEntity<T, Person> target,
                                                                   boolean copyIfNull) {
        if (copyIfNull || source.getRecorderPerson() != null) {
            if (source.getRecorderPerson() == null || source.getRecorderPerson().getId() == null) {
                target.setRecorderPerson(null);
            } else {
                target.setRecorderPerson(load(entityManager, Person.class, source.getRecorderPerson().getId()));
            }
        }
    }


    /* -- protected method -- */

    protected static <C> C load(EntityManager em, Class<? extends C> clazz, Serializable id) {
        return em.unwrap(Session.class).load(clazz, id);
    }

}
