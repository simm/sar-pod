package net.sumaris.core.dao.data;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 - 2020 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.dao.administration.user.DepartmentRepository;
import net.sumaris.core.dao.technical.Daos;
import net.sumaris.core.dao.technical.Pageables;
import net.sumaris.core.dao.technical.SortDirection;
import net.sumaris.core.dao.technical.jpa.SumarisJpaRepositoryImpl;
import net.sumaris.core.model.IUpdateDateEntity;
import net.sumaris.core.model.data.IDataEntity;
import net.sumaris.core.util.Beans;
import net.sumaris.core.vo.administration.user.DepartmentVO;
import net.sumaris.core.vo.data.IDataFetchOptions;
import net.sumaris.core.vo.data.IDataVO;
import net.sumaris.core.vo.filter.IDataFilter;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author peck7 on 30/03/2020.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@NoRepositoryBean
@Slf4j
public abstract class DataRepositoryImpl<E extends IDataEntity<Integer>, V extends IDataVO<Integer>, F extends IDataFilter, O extends IDataFetchOptions>
    extends SumarisJpaRepositoryImpl<E, Integer, V>
    implements DataRepository<E, V, F, O>, DataSpecifications<E> {

    private String[] copyExcludeProperties = new String[]{IUpdateDateEntity.Fields.UPDATE_DATE};

    @Autowired
    private DepartmentRepository departmentRepository;

    protected DataRepositoryImpl(Class<E> domainClass, Class<V> voClass, EntityManager entityManager) {
        super(domainClass, voClass, entityManager);
    }

    @Override
    public List<V> findAll(F filter) {
        return findAll(filter, (O)null);
    }

    @Override
    public List<V> findAll(F filter, O fetchOptions) {
        return findAll(toSpecification(filter, fetchOptions)).stream()
            .map(e -> this.toVO(e, fetchOptions))
            .collect(Collectors.toList());
    }

    @Override
    public Page<V> findAll(F filter, Pageable pageable) {
        return findAll(toSpecification(filter), pageable)
            .map(this::toVO);
    }

    @Override
    public Page<V> findAll(F filter, Pageable pageable, O fetchOptions) {
        return findAll(toSpecification(filter), pageable)
            .map(e -> this.toVO(e, fetchOptions));
    }

    @Override
    public List<V> findAll(F filter, net.sumaris.core.dao.technical.Page page, O fetchOptions) {
        return findAll(filter, page.asPageable(), fetchOptions)
                .stream().collect(Collectors.toList());
    }

    @Override
    public Page<V> findAll(int offset, int size, String sortAttribute, SortDirection sortDirection, O fetchOptions) {
        return findAll(Pageables.create(offset, size, sortAttribute, sortDirection))
            .map(e -> this.toVO(e, fetchOptions));
    }

    @Override
    public Page<V> findAll(F filter, int offset, int size, String sortAttribute, SortDirection sortDirection, O fetchOptions) {
        return findAll(toSpecification(filter), Pageables.create(offset, size, sortAttribute, sortDirection))
            .map(e -> this.toVO(e, fetchOptions));
    }

    @Override
    public List<V> findAllVO(@Nullable Specification<E> spec) {
        return super.findAll(spec).stream().map(this::toVO).collect(Collectors.toList());
    }

    @Override
    public Page<V> findAllVO(@Nullable Specification<E> spec, Pageable pageable) {
        return super.findAll(spec, pageable).map(this::toVO);
    }

    @Override
    public Page<V> findAllVO(@Nullable Specification<E> spec, Pageable pageable, O fetchOptions) {
        return super.findAll(spec, pageable).map(e -> this.toVO(e, fetchOptions));
    }

    @Override
    public List<V> findAllVO(@Nullable Specification<E> spec, O fetchOptions) {
        return super.findAll(spec).stream()
            .map(e -> this.toVO(e, fetchOptions))
            .collect(Collectors.toList());
    }

    @Override
    public long count(F filter) {
        return count(toSpecification(filter));
    }

    @Override
    public Optional<V> findById(int id) {
        return findById(id, null);
    }

    @Override
    public Optional<V> findById(int id, O fetchOptions) {
        return super.findById(id).map(entity -> toVO(entity, fetchOptions));
    }

    @Override
    public V get(Integer id) {
        return toVO(this.getById(id));
    }

    @Override
    public V get(Integer id, O fetchOptions) {
        return toVO(this.getById(id), fetchOptions);
    }

    @Override
    public V control(V vo) {
        Preconditions.checkNotNull(vo);
        E entity = getById(vo.getId());

        // Check update date
        if (isCheckUpdateDate()) Daos.checkUpdateDateForUpdate(vo, entity);

        // Lock entityName
        if (isLockForUpdate()) lockForUpdate(entity);

        Timestamp newUpdateDate = getDatabaseCurrentTimestamp();

        // Update update_dt
        entity.setUpdateDate(newUpdateDate);

        // Save entityName
        getEntityManager().merge(entity);


        // Update source
        vo.setControlDate(newUpdateDate);
        vo.setUpdateDate(newUpdateDate);

        return vo;
    }

    @Override
    public V validate(V vo) {
        throw new NotImplementedException("Not implemented yet");
    }

    @Override
    public V unValidate(V vo) {
        throw new NotImplementedException("Not implemented yet");
    }

    @Override
    public V qualify(V vo) {
        Preconditions.checkNotNull(vo);
        E entity = getById(vo.getId());

        // Check update date
        if (isCheckUpdateDate()) Daos.checkUpdateDateForUpdate(vo, entity);

        // Lock entityName
        if (isLockForUpdate()) lockForUpdate(entity);

        // Update update_dt
        Timestamp newUpdateDate = getDatabaseCurrentTimestamp();
        entity.setUpdateDate(newUpdateDate);

        // Save entityName
        getEntityManager().merge(entity);

        // Update source
        vo.setUpdateDate(newUpdateDate);

        return vo;
    }

    public void toEntity(V source, E target, boolean copyIfNull) {
        DataDaos.copyDataProperties(getEntityManager(), source, target, copyIfNull, getCopyExcludeProperties());
    }

    public V toVO(E source) {
        return toVO(source, null);
    }

    public V toVO(E source, O fetchOptions) {
        if (source == null) return null;
        V target = createVO();
        toVO(source, target, fetchOptions, true);
        return target;
    }

    @Override
    public void toVO(E source, V target, boolean copyIfNull) {
        toVO(source, target, null, copyIfNull);
    }

    public void toVO(E source, V target, O fetchOptions, boolean copyIfNull) {
        Beans.copyProperties(source, target);

        // Recorder department
        if (fetchOptions == null || fetchOptions.isWithRecorderDepartment()) {
            DepartmentVO recorderDepartment = departmentRepository.toVO(source.getRecorderDepartment());
            target.setRecorderDepartment(recorderDepartment);
        }
    }


    protected final Specification<E> toSpecification(F filter) {
        return toSpecification(filter, null);
    }

    protected Specification<E> toSpecification(F filter, O fetchOptions) {
        return hasRecorderDepartmentId(filter.getRecorderDepartmentId());
    }

    /* -- protected methods -- */

    protected String[] getCopyExcludeProperties() {
        return this.copyExcludeProperties;
    }

    protected void setCopyExcludeProperties(String... excludedProperties) {
        this.copyExcludeProperties = excludedProperties;
    }

}
