package net.sumaris.core.dao.referential;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import net.sumaris.core.model.administration.user.Department;
import net.sumaris.core.model.referential.*;
import net.sumaris.core.model.referential.location.Location;
import net.sumaris.core.model.referential.location.LocationClassification;
import net.sumaris.core.model.referential.location.LocationLevel;
import net.sumaris.core.model.referential.structure.CoastalStructureLevel;
import net.sumaris.core.model.referential.structure.CoastalStructureType;
import net.sumaris.core.model.referential.transcribing.TranscribingItem;
import net.sumaris.core.model.referential.transcribing.TranscribingItemType;
import net.sumaris.core.model.referential.transcribing.TranscribingSide;
import net.sumaris.core.model.referential.transcribing.TranscribingSystem;
import net.sumaris.core.model.technical.configuration.Software;
import net.sumaris.core.model.technical.versionning.SystemVersion;
import org.nuiton.i18n.I18n;
import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Helper class
 */
public class ReferentialEntities {

    // Reserved i18n (required when Nuiton init i18n files)
    static {
        I18n.n("sumaris.persistence.table.status");
        I18n.n("sumaris.persistence.table.department");
        I18n.n("sumaris.persistence.table.userProfile");
        I18n.n("sumaris.persistence.table.software");
        I18n.n("sumaris.persistence.table.systemVersion");
        I18n.n("sumaris.persistence.table.locationClassification");
        I18n.n("sumaris.persistence.table.locationLevel");
        I18n.n("sumaris.persistence.table.location");

        // Transcribing
        I18n.n("sumaris.persistence.table.transcribingSide");
        I18n.n("sumaris.persistence.table.transcribingSystem");
        I18n.n("sumaris.persistence.table.transcribingItemType");
        I18n.n("sumaris.persistence.table.transcribingItem");

        // Coastal structure
        I18n.n("sumaris.persistence.table.coastalStructureType");
        I18n.n("sumaris.persistence.table.coastalStructureLevel");
    }

    public static final List<Class<? extends IReferentialEntity>> REFERENTIAL_CLASSES = ImmutableList.of(
        Status.class,
        // Organization
        Department.class,
        UserProfile.class,
        // Construction
        CoastalStructureLevel.class,
        CoastalStructureType.class,
        // Software
        Software.class,
        // Transcribing
        TranscribingItem.class,
        TranscribingItemType.class,
        TranscribingSide.class,
        TranscribingSystem.class,
        // Technical
        ObjectType.class,
        SystemVersion.class,
        ProcessingType.class,
        ProcessingStatus.class,

        // Location
        LocationClassification.class,
        LocationLevel.class,
        Location.class
    );

    public static final Map<String, Class<? extends IReferentialEntity>> REFERENTIAL_CLASSES_BY_NAME = Maps.uniqueIndex(
            REFERENTIAL_CLASSES,
            Class::getSimpleName);

    public static final List<Class<? extends IReferentialEntity>> LAST_UPDATE_DATE_CLASSES_EXCLUDES = ImmutableList.of(
            // User
            UserProfile.class,
            // Software
            Software.class,
            // Technical
            SystemVersion.class
    );


    public static final Collection<String> LAST_UPDATE_DATE_ENTITY_NAMES = REFERENTIAL_CLASSES
            .stream().filter(c -> !LAST_UPDATE_DATE_CLASSES_EXCLUDES.contains(c))
            .map(Class::getSimpleName)
            .collect(Collectors.toList());

    public static final Map<String, PropertyDescriptor> LEVEL_PROPERTY_BY_CLASS_NAME = createLevelPropertyNameMap(REFERENTIAL_CLASSES);

    protected static final Map<String, PropertyDescriptor> createLevelPropertyNameMap(List<Class<? extends IReferentialEntity>> classes) {
        Map<String, PropertyDescriptor> result = new HashMap<>();

        // Detect level properties, by name
        classes.forEach((clazz) -> {
            PropertyDescriptor[] pds = BeanUtils.getPropertyDescriptors(clazz);
            Arrays.stream(pds)
                    .filter(propertyDescriptor -> propertyDescriptor.getName().matches("^.*[Ll]evel([A−Z].*)?$"))
                    .findFirst()
                    .ifPresent(propertyDescriptor -> result.put(clazz.getSimpleName(), propertyDescriptor));
        });

        // Other level (not having "level" in id)
        result.put(CoastalStructureType.class.getSimpleName(), BeanUtils.getPropertyDescriptor(CoastalStructureType.class, CoastalStructureType.Fields.LEVEL));
        result.put(LocationLevel.class.getSimpleName(), BeanUtils.getPropertyDescriptor(LocationLevel.class, LocationLevel.Fields.LOCATION_CLASSIFICATION));
        result.put(TranscribingItem.class.getSimpleName(), BeanUtils.getPropertyDescriptor(TranscribingItem.class, TranscribingItem.Fields.TYPE));
        result.put(TranscribingItemType.class.getSimpleName(), BeanUtils.getPropertyDescriptor(TranscribingItemType.class, TranscribingItemType.Fields.SYSTEM));

        return result;
    }


    public static Optional<String> getLevelPropertyName(String entityName) {
        return getLevelPropertyByClass(getEntityClass(entityName))
                .map(PropertyDescriptor::getName);
    }

    public static Class<? extends IReferentialEntity> getEntityClass(String entityName) {
        Preconditions.checkNotNull(entityName, "Missing 'entityName' argument");

        // Get entity class from entityName
        Class<? extends IReferentialEntity> entityClass = REFERENTIAL_CLASSES_BY_NAME.get(entityName);
        if (entityClass == null)
            throw new IllegalArgumentException(String.format("Referential entity [%s] not exists", entityName));

        return entityClass;
    }

    public static Optional<PropertyDescriptor> getLevelProperty(String entityName) {
        return Optional.ofNullable(LEVEL_PROPERTY_BY_CLASS_NAME.get(entityName));
    }

    public static Optional<PropertyDescriptor> getLevelPropertyByClass(Class<? extends IReferentialEntity> entityClass) {
        return getLevelProperty(entityClass.getSimpleName());
    }

    public static Optional<String> getLevelPropertyNameByClass(Class<? extends IReferentialEntity> entityClass) {
        return getLevelPropertyName(entityClass.getSimpleName());
    }
}
