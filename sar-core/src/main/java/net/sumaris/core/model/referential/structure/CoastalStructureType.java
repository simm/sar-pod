package net.sumaris.core.model.referential.structure;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import net.sumaris.core.model.ITreeNodeEntity;
import net.sumaris.core.model.annotation.OntologyEntity;
import net.sumaris.core.model.ModelVocabularies;
import net.sumaris.core.model.referential.IItemReferentialEntity;
import net.sumaris.core.model.referential.IWithUriEntity;
import net.sumaris.core.model.referential.Status;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "coastal_structure_type")
@Cacheable
@OntologyEntity(vocab = ModelVocabularies.COASTAL_STRUCTURE,
    query = "select t from CoastalStructureType as t" +
        " join fetch t.parent p" +
        " join fetch t.status s" +
        " join fetch t.level as level")
public class CoastalStructureType implements IItemReferentialEntity<Integer>,
    ITreeNodeEntity<Integer, CoastalStructureType>,
    IWithUriEntity<Integer> {

    public static final String SEQUENCE_NAME = "COASTAL_STRUCTURE_TYPE_SEQ";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName=SEQUENCE_NAME, allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @ToString.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_fk", nullable = false)
    private Status status;

    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    private String uri;

    @Column(nullable = false, length = IItemReferentialEntity.LENGTH_LABEL)
    @ToString.Include
    private String label;

    @Column(nullable = false, length = IItemReferentialEntity.LENGTH_NAME)
    private String name;

    @Column(nullable = true, length = IItemReferentialEntity.LENGTH_COMMENTS)
    private String description;

    @Column(length = IItemReferentialEntity.LENGTH_COMMENTS)
    private String comments;

    /* -- level -- */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coastal_structure_level_fk", nullable = false)
    private CoastalStructureLevel level;

    /* -- children and parent -- */

    @OneToMany(fetch = FetchType.LAZY, targetEntity = CoastalStructureType.class, mappedBy = CoastalStructureType.Fields.PARENT)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private List<CoastalStructureType> children = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = CoastalStructureType.class)
    @JoinColumn(name = "parent_coastal_structure_type_fk")
    private CoastalStructureType parent;

}
