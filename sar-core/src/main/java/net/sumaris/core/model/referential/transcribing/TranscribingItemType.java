package net.sumaris.core.model.referential.transcribing;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import net.sumaris.core.model.ModelVocabularies;
import net.sumaris.core.model.annotation.OntologyEntity;
import net.sumaris.core.model.referential.IItemReferentialEntity;
import net.sumaris.core.model.referential.IWithUriEntity;
import net.sumaris.core.model.referential.ObjectType;
import net.sumaris.core.model.referential.Status;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString(onlyExplicitlyIncluded = true)
@FieldNameConstants
@Entity
@Table(name = "transcribing_item_type")
@OntologyEntity(vocab = ModelVocabularies.TRANSCRIBING)
public class TranscribingItemType implements IItemReferentialEntity<Integer>, IWithUriEntity<Integer> {

    public static final String SEQUENCE_NAME = "TRANSCRIBING_ITEM_TYPE_SEQ";

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName=SEQUENCE_NAME, allocationSize = SEQUENCE_ALLOCATION_SIZE)
    @ToString.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_fk", nullable = false)
    private Status status;

    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    private String uri;

    @Column(nullable = false, length = LENGTH_LABEL)
    @ToString.Include
    private String label;

    @Column(nullable = false, length = LENGTH_NAME)
    private String name;

    @Column(nullable = true, length = IItemReferentialEntity.LENGTH_COMMENTS)
    private String description;

    @Column(length = LENGTH_COMMENTS)
    private String comments;

    @Column(name = "is_mandatory", nullable = false)
    private Boolean isMandatory;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = ObjectType.class)
    @JoinColumn(name = "object_type_fk", nullable = false)
    private ObjectType objectType;

    @Column(name = "object_reference_query", length = LENGTH_COMMENTS)
    private String objectReferenceQuery;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = TranscribingSide.class)
    @JoinColumn(name = "transcribing_side_fk", nullable = false)
    private TranscribingSide side;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = TranscribingSystem.class)
    @JoinColumn(name = "transcribing_system_fk")
    private TranscribingSystem system;
}
