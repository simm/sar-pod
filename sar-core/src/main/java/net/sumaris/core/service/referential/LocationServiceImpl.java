package net.sumaris.core.service.referential;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 - 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.dao.referential.ReferentialDao;
import net.sumaris.core.dao.referential.StatusRepository;
import net.sumaris.core.dao.referential.ValidityStatusRepository;
import net.sumaris.core.dao.referential.location.LocationClassificationRepository;
import net.sumaris.core.dao.referential.location.LocationLevelRepository;
import net.sumaris.core.dao.referential.location.LocationRepository;
import net.sumaris.core.dao.referential.location.Locations;
import net.sumaris.core.model.referential.location.Location;
import net.sumaris.core.model.referential.location.LocationClassification;
import net.sumaris.core.model.referential.location.LocationClassificationEnum;
import net.sumaris.core.model.referential.location.LocationLevel;
import net.sumaris.core.vo.filter.ReferentialFilterVO;
import net.sumaris.core.vo.referential.LocationVO;
import net.sumaris.core.vo.referential.ReferentialVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service("locationService")
@Slf4j
public class LocationServiceImpl implements LocationService{

    @Autowired
    protected LocationRepository locationRepository;

    @Autowired
    protected StatusRepository statusRepository;

    @Autowired
    protected ValidityStatusRepository validityStatusRepository;

    @Autowired
    protected LocationLevelRepository locationLevelRepository;

    @Autowired
    protected LocationClassificationRepository locationClassificationRepository;

    @Autowired
    protected ReferentialDao referentialDao;

    @Override
    public void updateLocationHierarchy() {
        if (log.isInfoEnabled()) {
            log.info("Updating location hierarchy...");
        }
        locationRepository.updateLocationHierarchy();
    }

    @Override
    public String getLocationLabelByLatLong(Number latitude, Number longitude) {
        if (longitude == null || latitude == null) {
            throw new IllegalArgumentException("Arguments 'latitude' and 'longitude' should not be null.");
        }

        // Try to find a statistical rectangle
        String rectangleLabel = Locations.getRectangleLabelByLatLong(latitude, longitude);
        if (StringUtils.isNotBlank(rectangleLabel)) return rectangleLabel;

        // TODO: find it from spatial query ?

        // Otherwise, return null
        return null;
    }

    @Override
    public Integer getLocationIdByLatLong(Number latitude, Number longitude) {
        String locationLabel = getLocationLabelByLatLong(latitude, longitude);
        if (locationLabel == null) return null;
        Optional<ReferentialVO> location = referentialDao.findByUniqueLabel(Location.class.getSimpleName(), locationLabel);
        return location.map(ReferentialVO::getId).orElse(null);
    }

    @Override
    public long countByLocationLevelId(int locationLevelId) {
        return locationRepository.countByLocationLevelId(locationLevelId);
    }

    @Override
    public List<LocationVO> saveAll(List<LocationVO> sources, boolean checkUpdateDate, boolean lockForUpdate) {
        return locationRepository.saveAll(sources, checkUpdateDate, lockForUpdate);
    }

    @Override
    public LocationVO save(LocationVO source) {
        return locationRepository.save(source);
    }

    /* -- protected -- */

    protected Map<String, LocationLevel> createAndGetLocationLevels(Map<String, String> levels) {
        Map<String, LocationLevel> result = Maps.newHashMap();

        Date creationDate = new Date();
        LocationClassification defaultClassification = locationClassificationRepository.getOne(LocationClassificationEnum.SEA.getId());

        for (String label: levels.keySet()) {
            String name = StringUtils.trimToNull(levels.get(label));

            LocationLevel locationLevel = locationLevelRepository.findByLabel(label);
            if (locationLevel == null) {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Adding new LocationLevel with label {%s}", label));
                }
                locationLevel = new LocationLevel();
                locationLevel.setLabel(label);
                locationLevel.setName(name);
                locationLevel.setCreationDate(creationDate);
                locationLevel.setLocationClassification(defaultClassification);
                locationLevel.setStatus(statusRepository.getEnableStatus());
                locationLevel = locationLevelRepository.save(locationLevel);
            }
            result.put(label, locationLevel);
        }
        return result;
    }

    protected List<LocationVO> getLocationsByLocationLevelId(int locationLevelId) {
        return locationRepository.findAll(ReferentialFilterVO.builder().levelId(locationLevelId).build());
    }
}
