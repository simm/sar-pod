package net.sumaris.core.service.technical;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 - 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.config.SumarisConfiguration;
import net.sumaris.core.config.SumarisConfigurationOption;
import net.sumaris.core.model.IEntity;
import net.sumaris.core.model.annotation.EntityEnum;
import net.sumaris.core.model.annotation.EntityEnums;
import net.sumaris.core.event.config.ConfigurationEventListener;
import net.sumaris.core.event.config.ConfigurationReadyEvent;
import net.sumaris.core.event.config.ConfigurationUpdatedEvent;
import net.sumaris.core.event.entity.AbstractEntityEvent;
import net.sumaris.core.event.entity.EntityDeleteEvent;
import net.sumaris.core.event.entity.EntityInsertEvent;
import net.sumaris.core.event.entity.EntityUpdateEvent;
import net.sumaris.core.event.schema.SchemaEvent;
import net.sumaris.core.event.schema.SchemaReadyEvent;
import net.sumaris.core.event.schema.SchemaUpdatedEvent;
import net.sumaris.core.exception.DenyDeletionException;
import net.sumaris.core.service.schema.DatabaseSchemaService;
import net.sumaris.core.util.Beans;
import net.sumaris.core.util.StringUtils;
import net.sumaris.core.vo.technical.SoftwareVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component("configurationService")
@Slf4j
public class ConfigurationServiceImpl implements ConfigurationService {

    private final SumarisConfiguration configuration;
    private final String currentSoftwareLabel;
    private boolean ready;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SoftwareService softwareService;

    @Autowired
    private DatabaseSchemaService databaseSchemaService;

    @Autowired
    private ApplicationEventPublisher publisher;

    private Version dbVersion;

    private final List<ConfigurationEventListener> listeners = new CopyOnWriteArrayList<>();

    @Autowired
    public ConfigurationServiceImpl(SumarisConfiguration configuration) {
        this.configuration = configuration;
        this.currentSoftwareLabel = configuration.getAppName();
        Preconditions.checkNotNull(currentSoftwareLabel);
        this.ready = !configuration.enableConfigurationDbPersistence(); // Mark as ready, if configuration not loaded from DB
    }

    @Override
    public SumarisConfiguration getConfiguration() {
        return configuration;
    }


    @Override
    public SoftwareVO getCurrentSoftware() {
        return softwareService.getByLabel(currentSoftwareLabel);
    }

    @Override
    public boolean isReady() {
        return ready;
    }

    @Override
    public void saveEnumeration(Enum enumValue) {

        EntityEnum entityEnum = enumValue.getClass().getAnnotation(EntityEnum.class);
        String joinProperty =  entityEnum.joinAttributes()[0];
        String entityName = entityEnum.entity().getSimpleName();
        String key = String.format("%s%s.%s.%s", entityEnum.configPrefix(),
            entityName,
            enumValue.name(),
            joinProperty);
        String value = Beans.getProperty(enumValue, joinProperty).toString();

        // Update the current software properties
        SoftwareVO software = getCurrentSoftware();
        Map<String, String> properties = software.getProperties();
        if (properties == null) {
            properties = new HashMap<>();
            software.setProperties(properties);
        }
        properties.put(key, value);
        save(software);
    }

    /* -- event listeners -- */

    protected SoftwareVO save(@NonNull SoftwareVO software) {
        Preconditions.checkNotNull(software.getLabel());
        Preconditions.checkNotNull(currentSoftwareLabel);
        Preconditions.checkNotNull(currentSoftwareLabel.equals(software.getLabel()));
        return softwareService.save(software);
    }

    @Async
    @EventListener({SchemaUpdatedEvent.class, SchemaReadyEvent.class})
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    protected void onSchemaUpdatedOrReady(SchemaEvent event) {
        if (this.dbVersion == null || !this.dbVersion.equals(event.getSchemaVersion())) {
            this.dbVersion = event.getSchemaVersion();

            if (!configuration.enableConfigurationDbPersistence()) {
                if (event instanceof SchemaReadyEvent && configuration.isProduction()) {
                    publishEvent(new ConfigurationReadyEvent(configuration));
                }
            }

            else {
                // Update the config, from the software properties
                applySoftwareProperties();

                // Publish ready event
                if (event instanceof SchemaReadyEvent) {
                    publishEvent(new ConfigurationReadyEvent(configuration));
                }
                // Publish update event
                else {
                    publishEvent(new ConfigurationUpdatedEvent(configuration));
                }
            }

            // Mark as ready
            ready = true;
        }
    }

    @Async
    @TransactionalEventListener(
            value = {EntityInsertEvent.class, EntityUpdateEvent.class},
            phase = TransactionPhase.AFTER_COMMIT,
            condition = "#event.entityName=='Software'")
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    protected void onSoftwareChanged(AbstractEntityEvent event) {

        if (!configuration.enableConfigurationDbPersistence()) return; // Skip

        // Check if should be applied into configuration
        SoftwareVO software = (SoftwareVO) event.getData();
        boolean isCurrentSoftware = (software != null && this.currentSoftwareLabel.equals(software.getLabel()));

        if (isCurrentSoftware) {
            ready = false;


            // Restore defaults
            configuration.restoreDefaults();

            // Update the config, from the software properties
            applySoftwareProperties();

            // Clean config cache
            configuration.cleanCache();

            // Publish update event
            publishEvent(new ConfigurationUpdatedEvent(configuration));

            // Mark as ready
            ready = true;
        }
    }

    @TransactionalEventListener(
            phase = TransactionPhase.BEFORE_COMMIT,
            condition = "#event.entityName=='Software'")
    @Transactional(propagation = Propagation.REQUIRED)
    public void beforeDeleteSoftware(EntityDeleteEvent event) {
        Preconditions.checkNotNull(event.getId());
        SoftwareVO currentSoftware = event.getData() != null ? (SoftwareVO)event.getData() : getCurrentSoftware();

        // Test if same as the current software
        boolean isCurrent = (currentSoftware != null && currentSoftware.getId().equals(event.getId()));

        // Avoid deletion of current software
        if (isCurrent) {
            throw new DenyDeletionException("Cannot delete the current software", ImmutableList.of(String.valueOf(currentSoftware.getId())));
        }
    }

    @Override
    public void addListener(ConfigurationEventListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(ConfigurationEventListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void applySoftwareProperties() {

        boolean newDatabase = false;

        // Resolved db version, if need
        if (this.dbVersion == null) {
            this.dbVersion = databaseSchemaService.getSchemaVersion().orElse(null);
            newDatabase = this.dbVersion == null;
        }

        // if new database or version > 0.9.5, then apply current software properties to config
        // else skip (because software tables not exists)
        Version minVersion = VersionBuilder.create("0.9.5").build();
        if (newDatabase || dbVersion.after(minVersion)) {
            applySoftwareProperties(configuration.getApplicationConfig(), getCurrentSoftware());
        }
        else {
            log.warn(String.format("Skip using software properties as config options, because schema version < %s. Waiting schema update...", minVersion.toString()));
        }

        // Refresh model enumerations, using config
        updateModelEnumerations();
    }

    /* -- protected methods -- */

    protected void applySoftwareProperties(ApplicationConfig appConfig, SoftwareVO software) {
        if (software == null) {
            log.info(String.format("No configuration for {%s} found in database. to enable configuration override from database, make sure to set the option '%s' to an existing row of the table SOFTWARE (column LABEL).", currentSoftwareLabel, SumarisConfigurationOption.APP_NAME.getKey()));
            return; // skip
        }

        Preconditions.checkNotNull(software.getLabel());

        Map<String, String> properties = software.getProperties();
        if (MapUtils.isEmpty(properties)) return; // Skip if empty

        log.info(String.format("Applying {%s} software properties, as config options...", software.getLabel()));

        // Load options from configuration providers
        Set<ApplicationConfigProvider> providers =
                ApplicationConfigHelper.getProviders(null,
                        null,
                        null,
                        true);
        Set<String> optionKeys = providers.stream()
                .map(ApplicationConfigProvider::getOptions)
                .flatMap(Stream::of)
                .map(ConfigOptionDef::getKey)
                .collect(Collectors.toSet());
        Set<String> transientOptionKeys = providers.stream()
                .map(ApplicationConfigProvider::getOptions)
                .flatMap(Stream::of)
                .filter(ConfigOptionDef::isTransient)
                .map(ConfigOptionDef::getKey)
                .collect(Collectors.toSet());

        boolean info = log.isInfoEnabled();

        properties.forEach((key, value) -> {
            if (!optionKeys.contains(key)) {
                if (info) log.debug(String.format(" - Skipping unknown configuration option {%s=%s}", key, value));
            } else if (transientOptionKeys.contains(key)) {
                if (info) log.warn(String.format(" - Skipping transient configuration option {%s=%s}", key, value));
            } else {
                if (info) log.info(String.format(" - Applying option {%s=%s}", key, value));
                appConfig.setOption(key, value);
            }
        });
    }

    protected void updateModelEnumerations() {

        ApplicationConfig appConfig = configuration.getApplicationConfig();

        boolean debug = log.isDebugEnabled();
        log.info("Updating model enumerations...");
        AtomicInteger counter = new AtomicInteger(0);
        AtomicInteger successCounter = new AtomicInteger(0);
        AtomicInteger errorCounter = new AtomicInteger(0);

        // For each enum classes
        EntityEnums.getEntityEnumClasses(configuration).forEach(enumClass -> {
            if (debug) log.debug(String.format("- Processing %s ...", enumClass.getSimpleName()));

            // Get annotation detail
            final EntityEnum annotation = enumClass.getAnnotation(EntityEnum.class);
            final String entityClassName = annotation.entity().getSimpleName();
            final String[] joinAttributes = annotation.joinAttributes();

            // Compute a option key (e.g. 'sumaris.enumeration.MyEntity.MY_ENUM_VALUE.id')
            String tempConfigPrefix = StringUtils.defaultIfBlank(annotation.configPrefix(), "");
            if (tempConfigPrefix.lastIndexOf(".") != tempConfigPrefix.length() - 1) {
                // Add trailing point
                tempConfigPrefix += ".";
            }
            final String configPrefix = tempConfigPrefix;

            final String queryPattern = String.format("from %s where %s = ?1",
                    entityClassName,
                    "%s");

            StringBuilder enumContentBuilder = new StringBuilder();
            StringBuilder configKeysBuilder = new StringBuilder();

            // For each enum values
            Arrays.stream(enumClass.getEnumConstants()).forEach(enumValue -> {
                counter.incrementAndGet();
                // Reset log buffer
                enumContentBuilder.setLength(0);
                configKeysBuilder.setLength(0);

                // Try to resolve, using each join attributes
                Optional<? extends IEntity> entity = Stream.of(joinAttributes).map(joinAttribute -> {
                    Object joinValue = Beans.getProperty(enumValue, joinAttribute);

                    if (joinValue == null) return null; // Skip this attribute

                    // If there is a config option, use it as join value
                    String configOptionKey = configPrefix + StringUtils.doting(entityClassName, enumValue.toString(), joinAttribute);
                    boolean hasConfigOption = appConfig.hasOption(configOptionKey);
                    if (hasConfigOption) {
                        joinValue = appConfig.getOption(joinValue.getClass(), configOptionKey);
                    }

                    // Find entities that match the attribute
                    List<? extends IEntity> matchEntities;
                    try {
                        matchEntities = entityManager.createQuery(String.format(queryPattern, joinAttribute), annotation.entity())
                            .setParameter(1, joinValue)
                            .getResultList();
                    }
                    catch (PersistenceException e) {
                        if (log.isDebugEnabled()) {
                            log.error("Unable to load entities for class {}: {}", entityClassName, e.getMessage(), e);
                        }
                        else {
                            log.error("Unable to load entities for class {}: {}", entityClassName, e.getMessage());
                        }
                        return null;
                    }

                    if (CollectionUtils.size(matchEntities) == 1) {
                        return matchEntities.get(0);
                    }
                    else {
                        if (IEntity.Fields.ID.equals(joinAttribute)) {
                            enumContentBuilder.append(", ").append(joinAttribute).append(": ").append(joinValue);
                        }
                        else {
                            enumContentBuilder.append(", ").append(joinAttribute).append(": '").append(joinValue).append("'");
                        }
                        configKeysBuilder.append(", '").append(configOptionKey).append("'");
                        return null;
                    }
                })
                        .filter(Objects::nonNull)
                        .findFirst();
                if (entity.isPresent()) {
                    successCounter.incrementAndGet();
                    if (debug) log.debug(String.format("Updating %s with %s", enumValue, entity.get()));

                    // Update the enum
                    Beans.copyProperties(entity.get(), enumValue);
                }
                else {
                    errorCounter.incrementAndGet();
                    log.warn(String.format(" - Missing %s{%s}. Please add it in database, or set configuration option %s",
                            entityClassName,
                            enumContentBuilder.length() > 2 ? enumContentBuilder.substring(2) : "null",
                            configKeysBuilder.length() > 2 ? configKeysBuilder.substring(2) : "<unknown>"));
                    Beans.setProperty(enumValue, IEntity.Fields.ID, EntityEnums.UNRESOLVED_ENUMERATION_ID);
                }
            });
        });

        String logMessage = String.format("Model enumerations updated (%s enumerations: %s updates, %s errors)",
                counter.get(),
                successCounter.get(),
                errorCounter.get());
        if (errorCounter.get() > 0) log.error(logMessage);
        else log.info(logMessage);
    }


    protected void publishEvent(ConfigurationUpdatedEvent event) {
        // Emit to Spring event bus
        publisher.publishEvent(event);

        // Emit to registered listeners
        for (ConfigurationEventListener listener: listeners) {
            try {
                listener.onUpdated(event);
            } catch (Throwable t) {
                log.error("Error on configuration updated event listener: " + t.getMessage(), t);
            }
        }
    }

    protected void publishEvent(ConfigurationReadyEvent event) {
        // Emit to Spring event bus
        publisher.publishEvent(event);

        // Emit to registered listeners
        for (ConfigurationEventListener listener: listeners) {
            try {
                listener.onReady(event);
            } catch (Throwable t) {
                log.error("Error on configuration ready event listener: " + t.getMessage(), t);
            }
        }
    }

}
