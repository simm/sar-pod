package net.sumaris.core.util.crypto;

/*-
 * #%L
 * SUMARiS:: Core
 * %%
 * Copyright (C) 2018 - 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

public class MD5UtilTest {

    @Test
    public void md5Hex() {
        String emailMd5 = MD5Util.md5Hex("demo@sar.milieumarinfrance.fr");
        Assert.assertEquals("5bbf8e5adc3bfc91350c5aeaf65d7f72", emailMd5);

        emailMd5 = MD5Util.md5Hex("obs@sar.milieumarinfrance.fr");
        Assert.assertEquals("9322b2009a81a6bf64e4c0ba44fd2854", emailMd5);

        emailMd5 = MD5Util.md5Hex("disable@sar.milieumarinfrance.fr");
        Assert.assertEquals("c38c511d56c03b9f434ef437ff5a33a7", emailMd5);

        emailMd5 = MD5Util.md5Hex("admin@sar.milieumarinfrance.fr");
        Assert.assertEquals("4bb4089b432109e8ef975ec6280a3bf8", emailMd5);

    }




}
