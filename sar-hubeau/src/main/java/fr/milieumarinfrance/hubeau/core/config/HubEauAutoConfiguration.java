/*
 * #%L
 * SUMARiS
 * %%
 * Copyright (C) 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.milieumarinfrance.hubeau.core.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.config.SumarisConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.ProxyProvider;

@Configuration
@Order(0)
@Slf4j
public class HubEauAutoConfiguration {

    @Bean
    @ConditionalOnProperty( name = "hubeau.enabled", matchIfMissing = true)
    public HubEauConfiguration hubEauConfiguration(SumarisConfiguration delegate) {
        log.info("Starting Hub'Eau module...");
        return new HubEauConfiguration(delegate);
    }

    @Bean
    @ConditionalOnProperty( name = "hubeau.enabled", matchIfMissing = true)
    public WebClient hubEauWebClient(HubEauConfiguration config) {
        String serverUrl = config.getServerUrl();
        String apiVersion = config.getApiVersion();
        String httpsProxy = System.getenv("HTTPS_PROXY") ;
        String httpsProxyPort = System.getenv("HTTPS_PROXY_PORT");

        log.info("Starting Hub'Eau client {{}} on API {{}}", serverUrl, apiVersion);

        // TODO : to be fixed in a regular way
        // HttpClient doesn't support -Dhttp.proxyHost and -Dhttp.proxyPort https://stackoverflow.com/questions/30168113/spring-boot-behind-a-network-proxy
        // Check here https://stackoverflow.com/questions/58689235/proxy-setting-not-working-with-spring-webclient and here : https://github.com/reactor/reactor-netty/issues/887
        // Here we get HTTP proxy conf via environment variable
        // Netty Proxy Provider Implementation doc : https://projectreactor.io/docs/netty/release/reference/index.html#_proxy_support_2
        if(httpsProxy != null && httpsProxyPort != null){
            log.info("HTTPS env variable are set, Host : {{}}, Port: {{}} Try using it for Hub'Eau client",httpsProxy,httpsProxyPort);
            // Try https://stackoverflow.com/questions/58689235/proxy-setting-not-working-with-spring-webclient
            HttpClient httpClient = HttpClient.create()
                    .proxy(proxy -> proxy.type(ProxyProvider.Proxy.HTTP)
                            .host(httpsProxy)
                            .port(Integer.parseInt(httpsProxyPort)))
                    .doOnConnected(conn -> conn
                            .addHandlerLast(new ReadTimeoutHandler(10))
                            .addHandlerLast(new WriteTimeoutHandler(10)));

            ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);

            return WebClient.builder()
                    .baseUrl(serverUrl)
                    .clientConnector(connector)
                    //.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    //.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                    .build();
        } else {
            HttpClient httpClient = HttpClient.create()
                    .tcpConfiguration(client ->
                            client.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
                                    .doOnConnected(conn -> conn
                                            .addHandlerLast(new ReadTimeoutHandler(10))
                                            .addHandlerLast(new WriteTimeoutHandler(10))));


            ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);

            return WebClient.builder()
                    .baseUrl(serverUrl)
                    .clientConnector(connector)
                    //.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    //.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                    .build();
        }
    }
}
