package fr.milieumarinfrance.hubeau.core.config;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.config.SumarisConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class HubEauConfiguration {

    private final SumarisConfiguration delegate;

    public HubEauConfiguration(SumarisConfiguration delegate){
        this.delegate = delegate;
    }

    public boolean isMock() {
        return delegate.getApplicationConfig().getOptionAsBoolean(HubEauConfigurationOption.HUBEAU_MOCK.getKey());
    }
    public String getServerUrl() {
        return delegate.getApplicationConfig().getOption(HubEauConfigurationOption.HUBEAU_API_URL.getKey());
    }
    public String getApiVersion() {
        return delegate.getApplicationConfig().getOption(HubEauConfigurationOption.HUBEAU_API_VERSION.getKey());
    }
    public String getApiBasePath() {
        return delegate.getApplicationConfig().getOption(HubEauConfigurationOption.HUBEAU_API_BASE_PATH.getKey());
    }
    public String getContaminantApiPath() {
        return delegate.getApplicationConfig().getOption(HubEauConfigurationOption.HUBEAU_API_CONTAMINANT_PATH.getKey());
    }
    public String getMonitoringLocationApiPath() {
        return delegate.getApplicationConfig().getOption(HubEauConfigurationOption.HUBEAU_API_MON_LOC_PATH.getKey());
    }

    public boolean enableMonitoringLocationCache() {
        return delegate.getApplicationConfig().getOptionAsBoolean(HubEauConfigurationOption.HUBEAU_MON_LOC_CACHE.getKey());
    }

}
