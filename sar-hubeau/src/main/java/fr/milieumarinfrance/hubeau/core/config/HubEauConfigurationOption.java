/*
 * #%L
 * SUMARiS
 * %%
 * Copyright (C) 2019 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.milieumarinfrance.hubeau.core.config;

import org.nuiton.config.ConfigOptionDef;

import static org.nuiton.i18n.I18n.n;

public enum HubEauConfigurationOption implements ConfigOptionDef {

    HUBEAU_MOCK(
        "hubeau.mock",
        n("sumaris.config.option.hubeau.mock.description"),
        "false",
        Boolean.class,
        false),

    HUBEAU_ENABLED(
        "hubeau.enabled",
        n("sumaris.config.option.hubeau.enabled.description"),
        Boolean.TRUE.toString(),
        Boolean.class,
        false),

    HUBEAU_API_URL(
        "hubeau.server.url",
        n("sumaris.config.option.hubeau.api.url.description"),
        //"https://hubeau.brgm-rec.fr",
        "https://hubeau.eaufrance.fr",
        String.class,
        false),

    HUBEAU_API_VERSION(
        "hubeau.api.version",
        n("sumaris.config.option.hubeau.api.version.description"),
        "v1",
        String.class,
        false),

    HUBEAU_API_BASE_PATH(
        "hubeau.api.basePath",
        n("sumaris.config.option.hubeau.api.path.description"),
        "/api/${hubeau.api.version}/",
        String.class,
        false),

    // Same as SumarisServerConfigurationOption.SERVER_URL
    HUBEAU_API_CONTAMINANT_PATH(
            "hubeau.api.contaminant.path",
            n("sumaris.config.option.hubeau.api.contaminant.path.description"),
            "${hubeau.api.basePath}surveillance_littoral/contaminants_chimiques",
            String.class,
            false),

    HUBEAU_API_MON_LOC_PATH(
            "hubeau.api.monitoringLocation.path",
            n("sumaris.config.option.hubeau.api.monLoc.path.description"),
            "${hubeau.api.basePath}surveillance_littoral/lieux_surv",
            String.class,
            false),

    HUBEAU_MON_LOC_CACHE(
        "hubeau.cache.monitoringLocation.enable",
        n("sumaris.config.option.hubeau.cache.monitoringLocation.enable.description"),
        Boolean.TRUE.toString(),
        Boolean.class,
        false),
    ;

    /** Configuration key. */
    private final String key;

    /** I18n key of option description */
    private final String description;

    /** Type of option */
    private final Class<?> type;

    /** Default value of option. */
    private String defaultValue;

    /** Flag to not keep option value on disk */
    private boolean isTransient;

    /** Flag to not allow option value modification */
    private boolean isFinal;

    HubEauConfigurationOption(String key,
                              String description,
                              String defaultValue,
                              Class<?> type,
                              boolean isTransient) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isTransient;
    }

    HubEauConfigurationOption(String key,
                              String description,
                              String defaultValue,
                              Class<?> type) {
        this(key, description, defaultValue, type, true);
    }

    /** {@inheritDoc} */
    @Override
    public String getKey() {
        return key;
    }

    /** {@inheritDoc} */
    @Override
    public Class<?> getType() {
        return type;
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return description;
    }

    /** {@inheritDoc} */
    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTransient() {
        return isTransient;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFinal() {
        return isFinal;
    }

    /** {@inheritDoc} */
    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /** {@inheritDoc} */
    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    /** {@inheritDoc} */
    @Override
    public void setFinal(boolean newValue) {
        // not used
    }

}
