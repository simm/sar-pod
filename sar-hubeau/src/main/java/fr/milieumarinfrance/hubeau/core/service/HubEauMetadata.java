package fr.milieumarinfrance.hubeau.core.service;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

public interface HubEauMetadata{

    String getJsonAliases(final Class<?> aClass, final Set<String> fields);

    String getJsonAlias(final Class<?> aClass, final String fieldName);

    Optional<String> formatDate(Date aDate);
}
