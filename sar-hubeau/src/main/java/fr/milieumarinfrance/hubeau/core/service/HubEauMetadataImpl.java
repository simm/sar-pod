package fr.milieumarinfrance.hubeau.core.service;

import com.fasterxml.jackson.annotation.JsonAlias;
import fr.milieumarinfrance.hubeau.core.cache.HubEauCacheConfiguration;
import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.util.Dates;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component("hubEauMetadata")
@ConditionalOnProperty( name = "hubeau.enabled", matchIfMissing = true)
@Slf4j
public class HubEauMetadataImpl implements HubEauMetadata {

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    public String getJsonAliases(final Class<?> aClass, final Set<String> fields) {
        return getJsonAliases(aClass, fields, ",");
    }

    @Cacheable(cacheNames = HubEauCacheConfiguration.Names.GET_JSON_ALIASES, condition = "#result==null")
    public String getJsonAliases(final Class<?> aClass, final Set<String> fields, String separator) {
        if (CollectionUtils.isEmpty(fields)) return null;
        return fields.stream()
            .map(f -> this.getJsonAlias(aClass, f))
            .collect(Collectors.joining(separator == null ? "," : separator));
    }

    @Cacheable(cacheNames= HubEauCacheConfiguration.Names.GET_JSON_ALIAS, condition = "#result==null")
    public String getJsonAlias(final Class<?> aClass, final String fieldName) {
        try {
            Field field = aClass.getDeclaredField(fieldName);
            final JsonAlias jsonAlias = field.getAnnotation(JsonAlias.class);
            if (jsonAlias != null) {
                return jsonAlias.value()[0];
            }
        }
        catch(Throwable t) {
            log.error("Cannot get {}.{} field metadata: {}", aClass.getSimpleName(), fieldName, t.getMessage());
        }
        return fieldName;
    }

    public Optional<String> formatDate(Date date) {
        if (date == null) return Optional.empty();
        return Optional.of(Dates.formatDate(date, DATE_PATTERN));
    }
}
