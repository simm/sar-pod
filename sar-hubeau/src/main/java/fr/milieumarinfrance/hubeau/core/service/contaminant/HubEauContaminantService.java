package fr.milieumarinfrance.hubeau.core.service.contaminant;

import fr.milieumarinfrance.hubeau.core.vo.ContaminantFilterVO;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantVO;
import fr.milieumarinfrance.hubeau.core.vo.ResultVO;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Transactional
public interface HubEauContaminantService {

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    ResultVO<ContaminantVO> findByFilter(ContaminantFilterVO filter, Pageable page, Set<String> fields);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String getDownloadLink(ContaminantFilterVO filter, Set<String> fields);
}
