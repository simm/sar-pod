package fr.milieumarinfrance.hubeau.core.service.contaminant;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.milieumarinfrance.hubeau.core.cache.HubEauCacheConfiguration;
import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import fr.milieumarinfrance.hubeau.core.service.HubEauMetadata;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantFilterVO;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantResultVO;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantVO;
import fr.milieumarinfrance.hubeau.core.vo.ResultVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.event.config.ConfigurationEvent;
import net.sumaris.core.event.config.ConfigurationReadyEvent;
import net.sumaris.core.event.config.ConfigurationUpdatedEvent;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;
import java.util.Set;

@Service("hubEauContaminantService")
@ConditionalOnProperty( name = "hubeau.enabled", matchIfMissing = true)
@Slf4j
public class HubEauContaminantServiceImpl implements HubEauContaminantService {

    private String apiGetPath;
    private String apiDownloadPath;

    private final HubEauConfiguration config;
    private final WebClient webClient;
    private final HubEauMetadata metadata;

    public HubEauContaminantServiceImpl(HubEauConfiguration hubEauConfiguration,
                                        WebClient hubEauWebClient,
                                        HubEauMetadata hubEauMetadata){
        log.info("Starting Hub'Eau contaminant service...");
        this.config = hubEauConfiguration;
        this.webClient = hubEauWebClient;
        this.metadata = hubEauMetadata;
    }

    @EventListener({ConfigurationReadyEvent.class, ConfigurationUpdatedEvent.class})
    public void onConfigurationReady(ConfigurationEvent event) {
        apiGetPath = config.getContaminantApiPath();
        apiDownloadPath = apiGetPath + ".csv";
    }

    @Override
    @Cacheable(cacheNames = HubEauCacheConfiguration.Names.CONTAMINANTS_BY_FILTER, condition = "#page.pageSize <= 200")
    public ResultVO<ContaminantVO> findByFilter(@NonNull ContaminantFilterVO filter,
                                                @NonNull Pageable page,
                                                Set<String> fields) {

        MutableObject<String> downloadUrl = new MutableObject<>();

        return webClient.get()
            .uri(apiGetPath, uriBuilder -> {

                uriBuilder
                    // filter
                    .queryParamIfPresent("code_parametre", Optional.ofNullable(filter.getParameterCode()))
                    .queryParamIfPresent("code_lieusurv", Optional.ofNullable(filter.getMonitoringLocationCode()))
                    // Date
                    .queryParamIfPresent("date_min_prel", metadata.formatDate(filter.getStartDate()))
                    .queryParamIfPresent("date_max_prel", metadata.formatDate(filter.getEndDate()))

                    // Geo
                    .queryParamIfPresent("latitude", Optional.ofNullable(filter.getLatitude()))
                    .queryParamIfPresent("longitude", Optional.ofNullable(filter.getLongitude()))
                    .queryParamIfPresent("bbox", Optional.ofNullable(filter.getBbox()))
                    .queryParamIfPresent("distance", Optional.ofNullable(filter.getDistance()))

                    .queryParamIfPresent("fields", Optional.ofNullable(metadata.getJsonAliases(ContaminantVO.class, fields)));

                // Build download path
                downloadUrl.setValue(uriBuilder.build()
                    .toString()
                    .replace(apiGetPath, apiDownloadPath));

                return uriBuilder
                        .queryParam("format", "json")
                        .queryParam("size", page.getPageSize())
                        .queryParam("page", page.getPageNumber() + 1)
                        .build();
                }
            )
            //.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(ContaminantResultVO.class)
            .map(result -> {
                result.setDownload(downloadUrl.getValue());
                return result;
            }).block();
    }

    /* -- protected functions -- */


    @Override
    public String getDownloadLink(ContaminantFilterVO filter, Set<String> fields) {
        return findByFilter(filter, Pageable.ofSize(0), fields).getDownload();
    }
}
