package fr.milieumarinfrance.hubeau.core.service.location;

import fr.milieumarinfrance.hubeau.core.vo.*;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.Set;

@Transactional
public interface HubEauMonitoringLocationService {

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    ResultVO<MonitoringLocationVO> findByFilter(MonitoringLocationFilterVO filter, Pageable page, Set<String> fields);

    @Transactional(propagation = Propagation.REQUIRED)
    void loadMonitoringLocations();
}
