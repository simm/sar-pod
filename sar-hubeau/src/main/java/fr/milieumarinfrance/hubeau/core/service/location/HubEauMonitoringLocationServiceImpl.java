package fr.milieumarinfrance.hubeau.core.service.location;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.milieumarinfrance.hubeau.core.cache.HubEauCacheConfiguration;
import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import fr.milieumarinfrance.hubeau.core.service.HubEauMetadata;
import fr.milieumarinfrance.hubeau.core.vo.MonitoringLocationFilterVO;
import fr.milieumarinfrance.hubeau.core.vo.MonitoringLocationResultVO;
import fr.milieumarinfrance.hubeau.core.vo.MonitoringLocationVO;
import fr.milieumarinfrance.hubeau.core.vo.ResultVO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.event.config.ConfigurationEvent;
import net.sumaris.core.event.config.ConfigurationReadyEvent;
import net.sumaris.core.event.config.ConfigurationUpdatedEvent;
import net.sumaris.core.exception.DataNotFoundException;
import net.sumaris.core.model.referential.StatusEnum;
import net.sumaris.core.model.referential.ValidityStatusEnum;
import net.sumaris.core.model.referential.location.*;
import net.sumaris.core.service.referential.LocationService;
import net.sumaris.core.service.referential.ReferentialService;
import net.sumaris.core.service.technical.ConfigurationService;
import net.sumaris.core.util.TimeUtils;
import net.sumaris.core.vo.filter.ReferentialFilterVO;
import net.sumaris.core.vo.referential.LocationVO;
import net.sumaris.core.vo.referential.ReferentialVO;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.*;
import java.util.stream.Collectors;

@Service("hubEauMonitoringLocationService")
@ConditionalOnProperty( name = "hubeau.enabled", matchIfMissing = true)
@Slf4j
public class HubEauMonitoringLocationServiceImpl implements HubEauMonitoringLocationService {

    private String apiGetPath;
    private String apiDownloadPath;
    private final HubEauConfiguration config;
    private final WebClient webClient;
    private final HubEauMetadata metadata;

    private final LocationService locationService;
    private final ReferentialService referentialService;
    private final ConfigurationService configurationService;

    public HubEauMonitoringLocationServiceImpl(HubEauConfiguration hubEauConfiguration,
                                               WebClient hubEauWebClient,
                                               HubEauMetadata metadata,
                                               LocationService locationService,
                                               ReferentialService referentialService,
                                               ConfigurationService configurationService){
        log.info("Starting Hub'Eau monitoring location service...");
        this.config = hubEauConfiguration;
        this.webClient = hubEauWebClient;
        this.metadata = metadata;
        this.locationService = locationService;
        this.referentialService = referentialService;
        this.configurationService = configurationService;
    }


    @EventListener({ConfigurationReadyEvent.class, ConfigurationUpdatedEvent.class})
    public void onConfigurationReady(ConfigurationEvent event) {
        apiGetPath = config.getMonitoringLocationApiPath();
        apiDownloadPath = apiGetPath + ".csv";
    }

    @Override
    @Cacheable(cacheNames = HubEauCacheConfiguration.Names.MONITORING_LOCATIONS_BY_FILTER, condition = "#page.pageSize <= 200")
    public ResultVO<MonitoringLocationVO> findByFilter(@NonNull MonitoringLocationFilterVO filter,
                                                       @NonNull Pageable page,
                                                       Set<String> fields) {

        MutableObject<String> downloadUrl = new MutableObject<>();

        return webClient.get()
            .uri(apiGetPath, uriBuilder -> {

                uriBuilder
                    // filter
                    .queryParamIfPresent("donnees_cc", Optional.ofNullable(filter.getHasContaminantData()))
                    .queryParamIfPresent("date_donnees", metadata.formatDate(filter.getDataDate()))
                    .queryParamIfPresent("date_min_maj", metadata.formatDate(filter.getMinUpdateDate()))
                    .queryParamIfPresent("date_max_maj", metadata.formatDate(filter.getMaxUpdateDate()))
                    // Geo
                    .queryParamIfPresent("latitude", Optional.ofNullable(filter.getLatitude()))
                    .queryParamIfPresent("longitude", Optional.ofNullable(filter.getLongitude()))
                    .queryParamIfPresent("bbox", Optional.ofNullable(filter.getBbox()))
                    .queryParamIfPresent("distance", Optional.ofNullable(filter.getDistance()))

                    .queryParamIfPresent("fields", Optional.ofNullable(metadata.getJsonAliases(MonitoringLocationVO.class, fields)));

                downloadUrl.setValue(uriBuilder.build().toString().replace(apiGetPath, apiDownloadPath));

                return uriBuilder
                    .queryParam("format", "json")
                    .queryParam("size", page.getPageSize())
                    .queryParam("page", page.getPageNumber() + 1)
                    .build();
                }
            )
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(MonitoringLocationResultVO.class)
            .map(result -> {
                result.setDownload(downloadUrl.getValue());
                return result;
            }).block();
    }

    @Override
    public void loadMonitoringLocations() {

        try {
            // Check location level exists
            checkLocationLevelExists(LocationClassificationEnum.HUBEAU, LocationLevelEnum.HUBEAU_MONITORING_LOCATION);

            int locationLevelId = LocationLevelEnum.HUBEAU_MONITORING_LOCATION.getId();
            long countMonLoc = locationService.countByLocationLevelId(locationLevelId);
            if (countMonLoc == 0) {
                updateMonitoringLocations(null);
            } else {
                Date lastUpdateDate = referentialService.getLastUpdateDate(Location.class.getSimpleName(), ReferentialFilterVO.builder()
                    .levelIds(new Integer[]{locationLevelId})
                    .build());
                updateMonitoringLocations(lastUpdateDate);
            }
        } catch(Exception e) {
            log.error("Error while loading monitoring locations", e);
            throw e;
        }
    }

    /* -- debug functions -- */


    private void updateMonitoringLocations(@Nullable Date minUpdateDate) {

        log.info("Loading HubEau monitoring locations... {minUpdateDate: {}}", metadata.formatDate(minUpdateDate).orElse("null"));
        int size = 200;
        Long now = System.currentTimeMillis();
        Pageable page = Pageable.ofSize(size);
        MonitoringLocationFilterVO filter = MonitoringLocationFilterVO.builder()
            .hasContaminantData(true)
            .minUpdateDate(minUpdateDate)
            .build();
        long count = 0;

        // Loop, while data fetched
        while (true) {
            ResultVO<MonitoringLocationVO> result = findByFilter(filter, page, null);
            MonitoringLocationVO[] sources = result.getData();

            int pageCount = Math.round(result.getCount() / size);
            if (pageCount > 0 && pageCount % 10 == 0) {
                log.info("Loading HubEau monitoring locations... {}/{}", page.getPageNumber(), pageCount);
            }

            if (ArrayUtils.isEmpty(sources)) break; // Stop

            List<LocationVO> targets = Arrays.stream(sources)
                .map(source -> toLocationVO(source, LocationLevelEnum.HUBEAU_MONITORING_LOCATION.getId()))
                .collect(Collectors.toList());
            locationService.saveAll(targets, false, false);

            // Prepare next iteration
            page = page.next();
            count += sources.length;
        }

        log.info("Loading HubEau monitoring locations [OK] - {} updates in {}",
            count,
            TimeUtils.printDurationFrom(now));

    }

    private LocationVO toLocationVO(MonitoringLocationVO source, int locationLevelId) {
        LocationVO target = new LocationVO();
        target.setId(Integer.parseInt(source.getCode()));
        target.setLabel(source.getLabel());
        target.setName(source.getName());
        target.setComments(source.getComment());
        target.setLevelId(locationLevelId);
        target.setCreationDate(source.getCreationDate());
        target.setUpdateDate(source.getUpdateDate());
        target.setBathymetry(source.getBathymetry());
        target.setUtFormat(source.getUtFormat());

        target.setStatusId(StatusEnum.ENABLE.getId()); // TODO find a way to get it from HubEau ?
        target.setValidityStatusId(ValidityStatusEnum.VALID.getId()); // TODO find a way to get it from HubEau ?

        return target;
    }

    private void checkLocationLevelExists(LocationClassificationEnum classification, LocationLevelEnum source) {

        // Check location classification exists
        checkLocationClassificationExists(classification);

        String entityName = LocationLevel.class.getSimpleName();
        ReferentialVO target = null;
        if (source.getId() != -1) {
            try {
                target = referentialService.get(entityName, source.getId());
            } catch (DataNotFoundException e) {
                // Continue
            }
        }
        else {
            target = referentialService.findByFilter(entityName, ReferentialFilterVO.builder()
                .label(source.getLabel())
                .build(), 0, 1, null, null).stream().findFirst().orElse(null);
        }
        if (target == null) {
            target = new ReferentialVO();
            target.setEntityName(entityName);
            target.setLabel(source.getLabel());
            target.setName("Lieux de surveillance (Hub'Eau)");
            target.setLevelId(classification.getId());
            target.setStatusId(StatusEnum.ENABLE.getId());

            // Save the new classification
            target = referentialService.save(target);

            // Update the source enum
            source.setId(target.getId());

            // Store new ID into the configuration (software properties)
            // This is required for enumeration ID override, when starting the POD
            configurationService.saveEnumeration(source);
        }
    }

    private void checkLocationClassificationExists(LocationClassificationEnum source) {
        String entityName = LocationClassification.class.getSimpleName();

        ReferentialVO target = null;
        if (source.getId() != -1) {
            try {
                target = referentialService.get(entityName, source.getId());
            } catch (DataNotFoundException e) {
                // Continue
            }
        }
        else {
            target = referentialService.findByFilter(entityName, ReferentialFilterVO.builder()
                .label(source.getLabel())
                .build(), 0, 1, null, null).stream().findFirst().orElse(null);
        }
        if (target == null) {
            target = new ReferentialVO();
            target.setEntityName(entityName);
            target.setLabel(source.getLabel());
            target.setName("Hub'Eau");
            target.setStatusId(StatusEnum.ENABLE.getId());

            // Save the new classification
            target = referentialService.save(target);

            // Update the source enum
            source.setId(target.getId());

            // Store new ID into the configuration (software properties)
            // This is required for enumeration ID override, when starting the POD
            configurationService.saveEnumeration(source);
        }
    }


}
