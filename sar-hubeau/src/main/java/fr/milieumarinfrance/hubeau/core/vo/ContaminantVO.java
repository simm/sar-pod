package fr.milieumarinfrance.hubeau.core.vo;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.geolatte.geom.Geometry;

import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldNameConstants
public class ContaminantVO {

    // Survey
    @JsonAlias("code_passage")
    private String surveyCode;
    @JsonAlias("mnemo_passage")
    private String surveyLabel;

    // Monitoring location
    @JsonAlias("code_lieusurv")
    private String monLocCode;
    @JsonAlias("libelle_lieusurv")
    private String monLocName;
    @JsonAlias("mnemo_lieusurv")
    private String monLocLabel;

    // Parameter
    @JsonAlias("code_parametre")
    private String parameterCode;
    @JsonAlias("libelle_parametre")
    private String parameterName;

    // Matrix
    @JsonAlias("code_support")
    private String matrixCode;
    @JsonAlias("libelle_support")
    private String matrixName;

    // Fraction
    @JsonAlias("code_fraction")
    private String fractionCode;
    @JsonAlias("libelle_fraction")
    private String fractionName;

    // Method
    @JsonAlias("code_methode")
    private String methodCode;
    @JsonAlias("libelle_methode")
    private String methodName;

    // Unit
    @JsonAlias("code_unite_resultat")
    private String unitCode;
    @JsonAlias("libelle_unite_resultat")
    private String unitName;

    // Programs
    @JsonAlias("codes_reseaux")
    private String[] programCodes;
    @JsonAlias("noms_reseaux")
    private String[] programNames;
    @JsonAlias("uri_reseaux")
    private String[] programUris;

    // Analysis instrument
    @JsonAlias("code_engin_prel")
    private String analysisInstrumentCode;
    @JsonAlias("libelle_engin_prel")
    private String analysisInstrumentName;

    // Position
    private Double longitude;
    private Double latitude;

    // Sampling operation
    @JsonAlias("date_prel")
    private Date samplingOperationDate;
    @JsonAlias("coordonnee_x_prel")
    private Double samplingOperationLongitude;
    @JsonAlias("coordonnee_y_prel")
    private Double samplingOperationLatitude;
    @JsonAlias("commentaire_prel")
    private String samplingOperationComment;


    // Department
    @JsonAlias("code_producteur")
    private String recorderDepartmentCode;
    @JsonAlias("nom_producteur")
    private String recorderDepartmentName;

    // Sample
    @JsonAlias("code_echantillon")
    private String sampleCode;
    @JsonAlias("mnemo_echantillon")
    private String sampleLabel;

    // Taxon
    @JsonAlias("code_taxon_echantillon")
    private String taxonCode;
    @JsonAlias("nomlatin_taxon_echantillon")
    private String taxonName;

    // Analysis department
    @JsonAlias("code_organisme_analyste")
    private String departmentCode;
    @JsonAlias("nom_organisme_analyste")
    private String departmentName;

    // Measure quality
    @JsonAlias("date_controle_analyse")
    private Date controlDate;
    @JsonAlias("date_validation_analyse")
    private Date validationDate;
    @JsonAlias("date_qualification_resultat")
    private Date qualificationDate;
    @JsonAlias("code_qualification_analyse")
    private String qualificationCode;
    @JsonAlias("commentaire_qualification_resultat")
    private String qualificationComment;

    @JsonAlias("resultat_analyse")
    private Double measurement;
    @JsonAlias("commentaire_analyse")
    private String comment;
}
