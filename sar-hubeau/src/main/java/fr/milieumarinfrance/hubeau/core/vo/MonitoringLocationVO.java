package fr.milieumarinfrance.hubeau.core.vo;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import net.sumaris.core.model.IUpdateDateEntity;
import net.sumaris.core.model.IValueObject;

import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldNameConstants
public class MonitoringLocationVO implements IValueObject<String>,
    IUpdateDateEntity<String, Date> {

    @JsonIgnore
    public String getId() {
        return code;
    }

    @JsonIgnore
    public void setId(String id) {
        this.code = id;
    }

    @JsonAlias("code_lieusurv")
    private String code;
    @JsonAlias("libelle_lieusurv")
    private String name;
    @JsonAlias("mnemo_lieusurv")
    private String label;
    @JsonAlias("ecart_heureTU_lieusurv")
    private Short utFormat;
    @JsonAlias("prof_lieusurv")
    private Double bathymetry;
    @JsonAlias("coordonnee_x_lieusurv")
    private String posX;
    @JsonAlias("coordonnee_y_lieusurv")
    private String posY;
    @JsonAlias("type_acquisition_coord")
    private String posAcquisitionType;
    @JsonAlias("codes_masses_eau")
    private String[] waterBodyCodes;
    @JsonAlias("noms_masses_eau")
    private String[] waterBodyNames;
    @JsonAlias("commentaire")
    private String comment;
    @JsonAlias("date_creation")
    private Date creationDate;
    @JsonAlias("date_maj")
    private Date updateDate;
    @JsonAlias("date_debut_donnees")
    private String startDate;
    @JsonAlias("date_fin_donnees")
    private String endDate;
    @JsonAlias("codes_reseaux")
    private String[] programCodes;
    @JsonAlias("noms_reseaux")
    private String[] programNames;
    @JsonAlias("uri_reseaux")
    private String[] programUris;
    @JsonAlias("codes_taxons_suivis")
    private String[] taxonCodes;
    @JsonAlias("noms_taxons_suivis")
    private String[] taxonNames;
    @JsonAlias("uri_taxons_suivis")
    private String[] taxonUris;

    @JsonAlias("donnees_cc")
    private boolean withChemicalContaminantData;

    @JsonAlias("longitude")
    private double longitude;
    @JsonAlias("latitude")
    private double latitude;
    @JsonAlias("geometry")
    private Map<String, Object> geometry; // Review this - use GeoJson ?

}
