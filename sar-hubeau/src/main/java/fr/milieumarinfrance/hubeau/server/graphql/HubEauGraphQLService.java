package fr.milieumarinfrance.hubeau.server.graphql;

/*-
 * #%L
 * SUMARiS:: Server
 * %%
 * Copyright (C) 2018 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import fr.milieumarinfrance.hubeau.core.service.contaminant.HubEauContaminantService;
import fr.milieumarinfrance.hubeau.core.service.location.HubEauMonitoringLocationService;
import fr.milieumarinfrance.hubeau.core.vo.*;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLEnvironment;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.execution.ResolutionEnvironment;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.dao.technical.Pageables;
import net.sumaris.core.dao.technical.SortDirection;
import net.sumaris.server.http.graphql.GraphQLApi;
import net.sumaris.server.http.graphql.GraphQLUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@GraphQLApi
@Service
@Transactional
@ConditionalOnBean({HubEauConfiguration.class})
@ConditionalOnWebApplication
@Slf4j
public class HubEauGraphQLService {

    private final HubEauContaminantService contaminantService;
    private final HubEauMonitoringLocationService monitoringLocationService;

    public HubEauGraphQLService(@Lazy HubEauContaminantService hubEauContaminantService,
                                @Lazy HubEauMonitoringLocationService hubEauMonitoringLocationService) {
        super();
        this.contaminantService = hubEauContaminantService;
        this.monitoringLocationService = hubEauMonitoringLocationService;
    }

    @GraphQLQuery(name = "contaminants", description = "Search in monitoring locations")
    @Transactional(readOnly = true)
    public ResultVO<ContaminantVO> findContaminants(
        @GraphQLArgument(name = "filter") ContaminantFilterVO filter,
        @GraphQLArgument(name = "offset", defaultValue = "0") Integer offset,
        @GraphQLArgument(name = "size", defaultValue = "100") Integer size,
        @GraphQLArgument(name = "sortBy", defaultValue = ContaminantVO.Fields.SURVEY_CODE) String sort,
        @GraphQLArgument(name = "sortDirection", defaultValue = "asc") String direction,
        @GraphQLEnvironment ResolutionEnvironment env) {

        return contaminantService.findByFilter(ContaminantFilterVO.nullToEmpty(filter),
            Pageables.create(offset == null ? 0 : offset,
                size == null ? 100 : size,
                sort == null ? ContaminantVO.Fields.SURVEY_CODE : sort,
                SortDirection.fromString(direction, SortDirection.ASC)),
            getDataFields(GraphQLUtils.fields(env)));
    }

    @GraphQLQuery(name = "monitoringLocations", description = "Search in monitoring locations")
    @Transactional(readOnly = true)
    public ResultVO<MonitoringLocationVO> findMonitoringLocation(
            @GraphQLArgument(name = "filter") MonitoringLocationFilterVO filter,
            @GraphQLArgument(name = "offset", defaultValue = "0") Integer offset,
            @GraphQLArgument(name = "size", defaultValue = "100") Integer size,
            @GraphQLArgument(name = "sortBy", defaultValue = MonitoringLocationVO.Fields.CODE) String sort,
            @GraphQLArgument(name = "sortDirection", defaultValue = "asc") String direction,
            @GraphQLEnvironment ResolutionEnvironment env) {

        return monitoringLocationService.findByFilter(MonitoringLocationFilterVO.nullToEmpty(filter),
            Pageables.create(offset == null ? 0 : offset,
                size == null ? 100 : size,
                sort == null ? MonitoringLocationVO.Fields.CODE : sort,
                SortDirection.fromString(direction, SortDirection.ASC)),
            getDataFields(GraphQLUtils.fields(env)));
    }

    protected Set<String> getDataFields(Set<String> fields) {
        return
            fields.stream()
            .filter(f -> f.startsWith("data/"))
            .map(f -> f.substring(5))
            .filter(f -> !f.startsWith("__"))
            .collect(Collectors.toSet());
    }
}
