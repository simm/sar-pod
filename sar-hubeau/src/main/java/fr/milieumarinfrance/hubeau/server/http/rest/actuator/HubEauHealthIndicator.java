package fr.milieumarinfrance.hubeau.server.http.rest.actuator;

import com.google.common.collect.ImmutableSet;
import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import fr.milieumarinfrance.hubeau.core.service.location.HubEauMonitoringLocationService;
import fr.milieumarinfrance.hubeau.core.vo.MonitoringLocationFilterVO;
import fr.milieumarinfrance.hubeau.core.vo.MonitoringLocationVO;
import fr.milieumarinfrance.hubeau.core.vo.ResultVO;
import net.sumaris.core.exception.SumarisTechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnClass({HealthIndicator.class})
@ConditionalOnWebApplication
@ConditionalOnProperty(
    prefix = "management.health.hubeau",
    name = "enabled",
    matchIfMissing = true
)
public class HubEauHealthIndicator implements HealthIndicator {
    private static final String ENDPOINT = "endpoint";
    private static final String API_VERSION = "apiVersion";
    private static final String ERROR = "error";

    @Autowired(required = false)
    private HubEauMonitoringLocationService monitoringLocationService;

    @Autowired(required = false)
    private HubEauConfiguration configuration;

    @Override
    public Health health() {

        if (monitoringLocationService == null || configuration == null) {
            return Health.down()
                .withDetail(ERROR, "Not Available").build();
        }

        String apiUrl = configuration.getServerUrl() + configuration.getApiBasePath();
        try {
            boolean ok = isAliveWithData();
            if (!ok) {
                return Health.down()
                    .withDetail(API_VERSION, configuration.getApiVersion())
                    .withDetail(ENDPOINT, apiUrl)
                    .withDetail(ERROR, "No data found")
                    .build();
            }
        } catch(Throwable t) {
            return Health.down()
                .withDetail(API_VERSION, configuration.getApiVersion())
                .withDetail(ENDPOINT, apiUrl)
                .withDetail(ERROR, t.getMessage())
                .build();
        }

        return Health.up()
            .withDetail(API_VERSION, configuration.getApiVersion())
            .withDetail(ENDPOINT, apiUrl)
            .build();
    }

    private boolean isAliveWithData() throws SumarisTechnicalException {
        MonitoringLocationFilterVO filter = MonitoringLocationFilterVO.builder()
            .hasContaminantData(true)
            .build();
        try {
            ResultVO<MonitoringLocationVO> result = monitoringLocationService.findByFilter(filter, Pageable.ofSize(1), ImmutableSet.of(
                MonitoringLocationVO.Fields.CODE
            ));
            return result != null && result.getCount() > 0;
        }
        catch (Throwable t) {
            throw new SumarisTechnicalException(t);
        }
    }
}