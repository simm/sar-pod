package fr.milieumarinfrance.hubeau.server.scheduler;

import fr.milieumarinfrance.hubeau.core.config.HubEauConfiguration;
import fr.milieumarinfrance.hubeau.core.service.location.HubEauMonitoringLocationService;
import lombok.extern.slf4j.Slf4j;
import net.sumaris.core.config.SumarisConfiguration;
import net.sumaris.core.event.config.ConfigurationEvent;
import net.sumaris.core.event.config.ConfigurationReadyEvent;
import net.sumaris.core.event.config.ConfigurationUpdatedEvent;
import net.sumaris.core.service.ServiceLocator;
import net.sumaris.core.util.Dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnBean({HubEauConfiguration.class})
@ConditionalOnWebApplication
@Slf4j
public class LoadHubEauReferentialJob {

    private final HubEauMonitoringLocationService service;
    private final HubEauConfiguration configuration;

    public LoadHubEauReferentialJob(HubEauConfiguration hubEauConfiguration,
                                    @Lazy HubEauMonitoringLocationService hubEauMonitoringLocationService) {
        super();
        this.configuration = hubEauConfiguration;
        this.service = hubEauMonitoringLocationService;
    }

    @EventListener({ConfigurationReadyEvent.class, ConfigurationUpdatedEvent.class})
    public void onConfigurationReady(ConfigurationEvent event) {

        if (configuration.enableMonitoringLocationCache()) {
            try {
                service.loadMonitoringLocations();
            }
            catch(Exception e) {
                // Continue
            }
        }
    }

    /* -- protected functions -- */

    @Scheduled(cron = "${hubeau.scheduling.daily.cron:0 0 0 * * ?}")
    protected void executeDaily(){
        if (configuration.enableMonitoringLocationCache()) {
            long now = System.currentTimeMillis();
            log.info("Updating Hub'Eau referential (daily scheduled job)...");

            service.loadMonitoringLocations();

            log.info("Updating Hub'Eau datasets [OK] {}", Dates.elapsedTime(now));
        }
    }
}
