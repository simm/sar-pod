package fr.milieumarinfrance.hubeau.core.service;

/*-
 * #%L
 * SAR :: Hub'Eau access
 * %%
 * Copyright (C) 2018 - 2021 Service d'Administration des Référentiels marins (SAR)
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.milieumarinfrance.hubeau.core.service.contaminant.HubEauContaminantService;
import fr.milieumarinfrance.hubeau.core.vo.ResultVO;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantFilterVO;
import fr.milieumarinfrance.hubeau.core.vo.ContaminantVO;
import net.sumaris.core.dao.technical.Pageables;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;

public class HubEauServiceTest extends AbstractServiceTest {

    @Resource
    private HubEauContaminantService hubEauContaminantService;



    @Test
    public void testGetData() {

        ContaminantFilterVO filter = ContaminantFilterVO.builder()
            .parameterCode("1203")
            .monitoringLocationCode("1001101")
            .build();

        ResultVO<ContaminantVO> result = hubEauContaminantService.findByFilter(filter,
            Pageables.create(0, 1, null, null),
            null);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getData());
        Assert.assertTrue(ArrayUtils.isNotEmpty(result.getData()));
    }
}
