#!/bin/bash

APP_NAME=sar
BASEDIR=/app
LOG_DIR=/app/logs
JAVA_OPTS="${JAVA_OPTS} -D${APP_NAME}.basedir=${BASEDIR}"
JAVA_OPTS="${JAVA_OPTS} -Dspring.config.location=file:${BASEDIR}/config/"
JAVA_OPTS="${JAVA_OPTS} -Dlog.file=${LOG_DIR}/${APP_NAME}-pod.log"
JAVA_OPTS="${JAVA_OPTS} -Doracle.net.tns_admin=/home/tnsnames"
JAVA_OPTS="${JAVA_OPTS} -Doracle.jdbc.timezoneAsRegion=false"
[[ "_${PROFILES}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dspring.profiles.active=${PROFILES}"
[[ "_${TZ}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Duser.timezone=${TZ}"
[[ "_${PORT}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dserver.port=${PORT}"
# ADD Http proxy support to deploy on validation and production @Ifremer
# https://stackoverflow.com/questions/30168113/spring-boot-behind-a-network-proxy
# We assume that if HTTP_PROXY is set, HTTP_PROXY_PORT also exist
[[ "_${HTTP_PROXY}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dhttp.proxyHost=${HTTP_PROXY} -Dhttp.proxyPort=${HTTP_PROXY_PORT}"
[[ "_${HTTPS_PROXY}" != "_" ]] && JAVA_OPTS="${JAVA_OPTS} -Dhttps.proxyHost=${HTTPS_PROXY} -Dhttps.proxyPort=${HTTPS_PROXY_PORT} -Dhttps.proxySet=true"
ARGS=
# TODO test this
#ARGS=${@:2}

echo "*** Starting ${APP_NAME}-pod - args: ${ARGS} - profiles: ${PROFILES} ***"

java ${JAVA_OPTS} -server -jar ${BASEDIR}/app.war ${ARGS}
