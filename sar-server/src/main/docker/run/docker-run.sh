#!/bin/bash

SCRIPT_DIR=$(dirname $0)
BASEDIR=$(cd "${SCRIPT_DIR}" && pwd -P)

VERSION=$1
PORT=$2
PROFILES=$3
CONFIG=${BASEDIR}/config
TIMEZONE=UTC
DEFAULT_VERSION=develop
DEFAULT_PORT=8088
DEFAULT_PROFILES=pgsql,dev

[[ "_${VERSION}" = "_" ]] && VERSION=${DEFAULT_VERSION}
[[ "_${PORT}" = "_" && "${VERSION}" = "${DEFAULT_VERSION}" ]] && PORT=${DEFAULT_PORT}
[[ "_${PROFILES}" = "_" && "${VERSION}" = "${DEFAULT_VERSION}" ]] && PROFILES=${DEFAULT_PROFILES}

CI_REGISTRY=gitlab-registry.ifremer.fr
CI_PROJECT_NAME=sar-pod
CI_PROJECT_PATH=simm/sar-pod
CI_REGISTRY_IMAGE_PATH=${CI_REGISTRY}/${CI_PROJECT_PATH}
CI_REGISTER_USER=gitlab+deploy-token
CI_REGISTER_PWD=ZZ7zDHLfPF6-GHNemUAP
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE_PATH}:${VERSION}
CONTAINER_PREFIX="${CI_PROJECT_NAME}-${PORT}"

# FIXME BLA: add version in container name ?
#CONTAINER_NAME="${CONTAINER_PREFIX}-${VERSION}"
CONTAINER_NAME="${CONTAINER_PREFIX}"

# Check arguments
#if [[ (! $VERSION =~ ^[0-9]+.[0-9]+.[0-9]+(-(alpha|beta|rc|SNAPSHOT)[-0-9]*)?$ && $VERSION != 'imagine' ) ]]; then
#  echo "ERROR: Invalid version"
#  echo " Usage: $0 <version> <port>"
#  exit 1
#fi
if [[ (! $PORT =~ ^[0-9]+$ ) ]]; then
  echo "ERROR: Invalid port"
  echo " Usage: $0 <version> <port>"
  exit 1
fi

# Log start
echo "--- Starting ${CI_PROJECT_NAME} v${APP_VERSION} on port ${PORT} (profiles: '${PROFILES}', config: '${CONFIG})'}"

## Login to container registry
echo "Login to ${CI_REGISTRY}..."
export CR_TOKEN=${CI_REGISTER_PWD}
echo $CR_TOKEN | docker login ${CI_REGISTRY} --password-stdin -u ${CI_REGISTER_USER}
[[ $? -ne 0 ]] && exit 1

# Pull the expected image
echo "Pulling image ${CI_REGISTRY_IMAGE}"
docker pull ${CI_REGISTRY_IMAGE}
[[ $? -ne 0 ]] && exit 1

# Logout from container registry
docker logout ${CI_REGISTRY}

# Stop existing container
if [[ ! -z  $(docker ps -f name=${CONTAINER_PREFIX} -q) ]]; then
  echo "Stopping running instance..."
  docker stop $(docker ps -f name=${CONTAINER_PREFIX} -q)
fi

if [[ ! -d "${CONFIG}" ]]; then
  echo "ERROR: Config (directory or file) not found: '${CONFIG}'"
  exit 1
fi

# Waiting container really removed
sleep 3

docker run -it -d --rm \
           --name "${CONTAINER_NAME}" \
           -p ${PORT}:${PORT} \
           -v ${CONFIG}:/app/config   \
           -v /home/tnsnames:/home/tnsnames \
           -e PROFILES=${PROFILES} \
           -e PORT=${PORT} \
           -e TZ=${TIMEZONE} \
           ${CI_REGISTRY_IMAGE}

echo "---- ${CI_PROJECT_NAME} is running !"
echo ""
echo " Available commands:"
echo "    logs: docker logs -f ${CONTAINER_NAME}"
echo "    bash: docker exec -it ${CONTAINER_NAME} bash"
echo "    stop: docker stop ${CONTAINER_NAME}"
echo "  status: docker ps -a ${CONTAINER_NAME}"
