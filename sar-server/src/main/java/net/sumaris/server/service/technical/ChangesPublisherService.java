package net.sumaris.server.service.technical;

/*-
 * #%L
 * SUMARiS:: Server
 * %%
 * Copyright (C) 2018 SUMARiS Consortium
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.sumaris.core.model.IUpdateDateEntity;
import org.reactivestreams.Publisher;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

@Transactional
public interface ChangesPublisherService {

    @Transactional(readOnly = true)
    <K extends Serializable, D extends Date, V extends IUpdateDateEntity<K, D>, L extends List<V>> Publisher<L>
    getListPublisher(final Function<Date, L> supplier,
                 Integer minIntervalInSecond,
                 boolean startWithActualValue);

    @Transactional(readOnly = true)
    <K extends Serializable, D extends Date, V extends IUpdateDateEntity<K, D>> Publisher<V>
    getPublisher(final Function<Date, V> supplier,
                 Integer minIntervalInSecond,
                 boolean startWithActualValue);

    @Transactional(readOnly = true)
    <K extends Serializable, D extends Date, T extends IUpdateDateEntity<K, D>, V extends IUpdateDateEntity<K, D>> Publisher<V>
    getPublisher(Class<T> entityClass,
                 Class<V> targetClass,
                 K id,
                 Integer minIntervalInSecond,
                 final boolean startWithActualValue);

    @Transactional(readOnly = true)
    <K extends Serializable, D extends Date, T extends IUpdateDateEntity<K, D>, V extends IUpdateDateEntity<K, D>> V
    getIfNewer(Class<T> entityClass,
               Class<V> targetClass,
               K id,
               Date lastUpdateDate);
}
