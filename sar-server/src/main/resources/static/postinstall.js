#!/usr/bin/env node

const fs = require('fs'),
path = require('path');

const jsDependencies = {
  'node_modules/jquery/dist/': ['jquery.min.js', 'jquery.min.map'],
  'node_modules/bootstrap/dist/js/': ['bootstrap.min.js', 'bootstrap.min.js.map'],
  'node_modules/@popperjs/core/dist/umd/': ['popper.min.js', 'popper.min.js.map']
};
const cssDependencies = {
  'node_modules/bootstrap/dist/css/': [
    'bootstrap.css', 'bootstrap.css.map', 'bootstrap.min.css', 'bootstrap.min.css.map',
    'bootstrap-grid.css', 'bootstrap-grid.css.map', 'bootstrap-grid.min.css', 'bootstrap-grid.min.css.map',
    'bootstrap-utilities.css', 'bootstrap-utilities.css.map', 'bootstrap-utilities.min.css', 'bootstrap-utilities.min.css.map',
  ],
  'node_modules/@expo-google-fonts/': ['asap/*.ttf'],
};

function isDirectory(path) {
  const stats = fs.existsSync(path) && fs.statSync(path);
  return stats && stats.isDirectory();
}
function renameFile(src, dest) {
  try {
    if (isDirectory(src)) {
      if (isDirectory(dest)) {
        copyDirWithPattern(src + '/*', dest);
        fs.rmdirSync(src, { recursive: true });
      }
      else {
        fs.renameSync(src, dest);
      }
    }
    else {
      fs.renameSync(src, dest);
    }
  } catch (e) {
    throw new Error(e);
  }
}

function copyFile(src, dest) {
  const destDir = dest.substr(0, dest.lastIndexOf('/')+1);
  fs.mkdirSync(destDir, { recursive: true });
  try {
    fs.copyFileSync(src, dest, { force: true });
  } catch (e) {
    throw new Error(e);
  }
}
function copyDir(src, dest) {
  fs.mkdirSync(dest, { recursive: true });
  let entries =  fs.readdirSync(src, { withFileTypes: true });

  for (let entry of entries) {
    let srcPath = path.join(src, entry.name);
    let destPath = path.join(dest, entry.name);

    entry.isDirectory() ?
      copyDir(srcPath, destPath) :
      copyFile(srcPath, destPath);
  }
}
function copyDirWithPattern(src, dest) {
  const srcPattern = new RegExp('^' + src.replace(/[.]/g, '[.]').replace(/[*]/g, '.*') + '$');
  const srcDir = src.substr(0, src.lastIndexOf('/')+1);
  const destDir = dest.substr(0, dest.lastIndexOf('/')+1);

  fs.mkdirSync(destDir, { recursive: true });
  let entries =  fs.readdirSync(srcDir, { withFileTypes: true });

  for (let entry of entries) {
    let srcPath = path.join(srcDir, entry.name);
    let destPath = path.join(destDir, entry.name);

    if (srcPattern.test(srcPath)) {
      entry.isDirectory() ?
        copyDir(srcPath, destPath) :
        copyFile(srcPath, destPath);
    }
  }
}
function copyDependencies(dependencies, targetDir) {
  Object.keys(dependencies).forEach(dependency => {
    const dir = dependency.lastIndexOf('/') === dependency.length - 1 ? dependency : (dependency + '/');  // Add trailing slash
    dependencies[dependency].forEach(file => {
      const source = path.resolve(dir + file);
      const target = path.resolve(targetDir + file);
      if (source.indexOf('*') !== -1) {
        copyDirWithPattern(source, target);
      }
      else if (isDirectory(source)) {
        copyDir(source, target);
      }
      else {
        copyFile(source, target);
      }
    });
  });
}

function install() {
  // Copy JS files
  copyDependencies(jsDependencies, './api/js/');

  // Copy CSS files
  copyDependencies(cssDependencies, './api/css/');

  renameFile('./api/css/asap', './api/css/fonts');
}

install();
