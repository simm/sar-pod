#!/bin/bash

# Get to the root project
if [[ "_" == "_${PROJECT_DIR}" ]]; then
  SCRIPT_DIR=$(dirname $0)
  PROJECT_DIR=$(cd "${SCRIPT_DIR}/../../.." && pwd)
  export PROJECT_DIR
fi;

# ------------------------------------
# Init variables
LOG_PREFIX="--------------"
#MVN_INSTALL_OPTS="-DskipTests --quiet --offline"
MVN_INSTALL_OPTS="-DskipTests --quiet"
PROJECT_ROOT=$(cd ${PROJECT_DIR}/.. && pwd)
HOME=`eval echo "~$USER"`
APP_BASEDIR="${PROJECT_ROOT}/.local"
LOG_DIR="${APP_BASEDIR}/log/sar-pod.log"
DB_URL="jdbc:hsqldb:hsql://localhost:9001/sar"
#CONFIG_DIR="${PROJECT_ROOT}/.local/config/"
PROFILE=hsqldb
VERSION=`grep -m1 -P "\<version>[0-9A−Z.]+(-\w*)?</version>" ${PROJECT_DIR}/pom.xml | grep -oP "\d+.\d+.\d+(-\w*)?"`
WAR_FILE="${PROJECT_DIR}/target/sar-server-${VERSION}.war"
JAVA_OPTS=""

f_help() {
  cat << EOF

$(basename $0) :

  -s | --server-ip : set the ipv4 for -Dserver.url -Dserver.address
  -p | --server-port : set the port for -Dserver.url (8080 by default)

  -- Others args for java

EOF
}

eval set -- $(getopt -o 's:p:h' --long 'server-ip:,server-port,help' -n $(basename $0) -- "$@")
while true ; do
  case "${1}" in
    '-h'|'--help')
      f_help
      exit
      ;;
    '-s'|'--serve')
      shift
      if [[ ! "${1}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] ; then
        echo "ERROR: ${1} is not suitable ip"
        exit 1
      fi
      SERVER_IP="${1}"
      ;;
    '-p'|'--serve-port')
      shift
      if [[ ! "${1}" =~ ^[0-9]{1,5}$ ]] || [ "${1}" -gt 65535 ] ; then
        echo "ERROR: ${1} is not suitable port number"
        exit 1
      fi
      SERVER_PORT="${1}"
      ;;
    '--')
      shift
      JAVA_OPTS+="$@ "
      break
      ;;
  esac
  shift
done

if [ -n "${SERVER_IP}" ] ; then
  JAVA_OPTS+="-Dserver.url=http://${SERVER_IP}:${SERVER_PORT:-8080} -Dserver.address=${SERVER_IP} -Dserver.port=${SERVER_PORT:-8080}"
fi

echo "INFO: Project root: ${PROJECT_ROOT}"
mkdir -p ${APP_BASEDIR}

# ------------------------------------
echo "${LOG_PREFIX} Installing [core-shared], [core] and [server]... ${LOG_PREFIX}"
# ------------------------------------
cd ${PROJECT_ROOT}
if [ ! -f "${WAR_FILE}"] ; then
  mvn install -pl sar-core-shared,sar-core,sar-core-rdf,sar-hubeau,sar-server $MVN_INSTALL_OPTS
else
  echo "INFO: War file ${WAR_FILE} found : do not re build"
fi
[[ $? -ne 0 ]] && exit 1

# Makse sure war file exists
if [[ ! -f "${WAR_FILE}" ]]; then
  echo "ERROR: Cannot found war file, at '${WAR_FILE}'"
  exit 1
fi;

cd ${PROJECT_DIR}

JAVA_OPTS+=" -Xms512m -Xmx1024m"
JAVA_OPTS+=" -Dspring.main.banner-mode=off"
JAVA_OPTS+=" -Dsumaris.basedir=${APP_BASEDIR}"
JAVA_OPTS+=" -Dlog.file=${LOG_DIR}"
JAVA_OPTS+=" -Dspring.datasource.url=${DB_URL}"
JAVA_OPTS+=" -Dsumaris.server.keypair.salt=abc"
JAVA_OPTS+=" -Dsumaris.server.keypair.password=def"
JAVA_OPTS+=" -Drdf.enabled=true"
if [[ -d "${CONFIG_DIR}" ]]; then
  JAVA_OPTS+=" -Dspring.config.location=${PROJECT_ROOT}/.local/config/"
fi;
[[ "_${PROFILE}" != "_" ]]  && JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=${PROFILE}"

JAVA_CMD="java ${JAVA_OPTS} -jar ${WAR_FILE}"

# ------------------------------------
echo "${LOG_PREFIX} Running pod from '${WAR_FILE}'... ${LOG_PREFIX}"
echo "Executing command: ${JAVA_CMD}"
echo "${LOG_PREFIX}"
# ------------------------------------
${JAVA_CMD}
