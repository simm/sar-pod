


# Installation (App)

The **SAR App** is a [client software](https://en.wikipedia.org/wiki/Client_(computing)) connecting to any [SAR Pod](./pod.md):

### Install on a desktop computer (web forms)

 1. Download the last ZIP archive at: https://github.com/simm-sar/sar-app/releases;
 2. Unzip the downloaded archive anywhere;
 3. Open the files `ìndex.html` in a web browser (Firefox or Chrome);
 4. That's it !
    

