# Databases

## Installation

- Create schema scripts :
  - PostgreSQL : [schema-create-pgsql.sql](./sql/schema-create-pgsql.sql) 

## Model

Available documentation :

- [Conceptual model](https://gitlab.ifremer.fr/simm/sar-doc/-/tree/master/model) (hosted by 'sar-doc' GitLab project)

- [Tables](./sar-core/hibernate/tables/index.html);

- [Entities](./sar-core/hibernate/entities/index.html) Conceptual model and queries (used by Hibernate Framework).


