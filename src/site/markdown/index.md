# Home

![](./images/logos/logo-sar-400px.png)

Welcome to the **documentation web site of SAR**!


## Main features

SAR comes with:

 - Data entry web forms for desktop:
  
    * manage users (access rights per data collection program) ;
    
    * manage referential: humans constructions, ...;

## Documentation  

 - [Database tables](./sar-core/hibernate/tables/index.html) with all associated columns;

## Installation
  
- [App installation](app.md) 
- [Pod and database installation](pod.md)

## License

SAR is Free Software (GPL License). Source code and documentation are under [License GPL v3](LICENSE.md).

## How You Can Help

In the free software world, there is generally no distinction between users and developers.
As in a friendly neighbourhood, everybody pitches in to help their neighbors.
Please consider the time you give in assistance to others as payment.

Ways in which you can help:

 - Test existing features and provide feedback,
 - [Report bugs or ask for enhancements](https://github.com/simm/sar-app/issues),
 - Add documentation,
 - Translate SUMARiS to your own language,
 - Translate the documentation,
 - Improve this website,
 - Help others to learn to use SUMARiS, etc.


## Project's partners

SAR have been developed by the following consortium:

[![](./images/logos/logo-simm.png)](https://www.milieumarinfrance.fr/)
[![](./images/logos/logo-ifremer.png)](https://www.ifremer.fr)
[![](./images/logos/logo-ofb.png)](https://ofb.gouv.fr/)
[![](./images/logos/logo-mte.png)](https://www.ecologie.gouv.fr/)
[![](./images/logos/logo-eis.png)](https://www.e-is.pro)
