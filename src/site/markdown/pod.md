

# Database and Pod

SAR tools use a database engine, to store data.

A SAR database instance is accessible through a [server software](https://en.wikipedia.org/wiki/Server_(computing)), called **SAR Pod**.

## Main features of the Pod

The SAR Pod has several features:

 - Create a database instance, then manage schema updates.
    * The Pod is compliant with many database engines: [PostgreSQL](https://www.postgresql.org/) or [HSQLDB](http://hsqldb.org/) ;

 - Allow data access (read/write) to a database instance. Such API is used by client software (like the [SAR App](./app.md)):
    * Publish data as GraphQL API;
    * Publish data as RDF API for semantic web, including OWL (Ontology Web Language);

## Database model

 - [Conceptual model](doc/model/index.md) of the database:
 - [Tables list](./sar-core/hibernate/tables/index.html) with all associated columns;

 - For IT developers: 
    * [Entities](./sar-core/hibernate/entities/index.html) (Hibernate mapping);
    * [HQL named queries](./sar-core/hibernate/queries/index.html) declared in the source code.
      We also use JPA Criteria API to build queries dynamically (see source code for more details).

## Installation of the database

### PostgreSQL engine (default)

- Install a PosgreSQL database engine (v15+)
- Create the schema, using the file [schema-create-pgsql.sql](./sql/schema-create-pgsql.sql)

### HSQLDB engine

- Copy the file [sar-db-hsqldb.sh](https://github.com/simm-sar/sar-pod/blob/master/sar-server/src/main/assembly/bin/sar-db-hsqldb.sh) locally

```bash
wget -kL https://github.com/simm-sar/sar-pod/blob/master/sar-server/src/main/assembly/bin/sar-db-hsqldb.sh
# Or using curl: 
# curl https://github.com/simm-sar/sar-pod/blob/master/sar-server/src/main/assembly/bin/sar-db-hsqldb.sh > sar-db-hsqldb.sh  

# Give execution rights
chmod u+x sar-db-hsqldb.sh
```

- Edit this file, to set the `SAR_HOME` variable :
```bash
#!/bin/bash
# --- User variables (can be redefined): ---------------------------------------
#SAR_HOME=/path/to/sar/home
SERVICE_NAME=sar-db
DB_NAME=sar
DB_PORT=9000
(...)
```

- Start the database, using the command (in a terminal): 
```
./sar-db-hsqldb.sh start
```  

- That's it !
  
  Your database is ready, and should be accessible (e.g. through a JDBC client software).
 
  To make sure everything is running well, check logs at: `<SAR_HOME>/logs/` 


## Installation of the Pod

### On Linux systems (Debian, Ubuntu)

 1. Install Pod's dependencies: 
    * Install LibSodium (Unix only) : https://download.libsodium.org/doc/installation/
    * Install Java SDK 11
    
 2. Download the latest JAR file at: https://github.com/simm-sar/sar-pod/releases

 3. Copy the JAR file anywhere;
 
 4. In a terminal, start the pod using the command:
    ```bash
    java -jar sar-pod-x.y.z.jar
    ``` 

  5. Congratulations ! 
  
     Your Pod should now be running..
     
     A welcome page should also be visible at the address [http://localhost:8080](http://localhost:8080):
     
     ![](./images/pod-screenshot-api.png)

<u>Note for IT developers:</u> 

Your running Pod give access to useful dev tools : 
  - A GraphQL live query editor, at `<server_url>/grapiql` (WARN some query will need authorization) 
  - A GraphQL subscription query editor (GraphQL + websocket), at `<server_url>/subscription/test`
    
### On MS Windows

`TODO: write this part`

### Configuration

To change the Pod's configuration, follow this steps:

 1. Create a directory for your configuration (e.g. `/home/<USER>/.config/sar/`): 
    ```bash
    mkdir -p /home/<USER>/.config/sar/
    ```
 
 2. Create a file `application.yml` inside this directory;
 
 3. Edit the file, and add options you want to override (see the [list of available options](./config-report.html)):
 
    A basic example:
    ```yml
    server.address: 127.0.0.1
    server.port: 8080  
    sar.basedir: /home/<USER>/.config/sar
    ```

 4. In a terminal, start the pod with the command:
    ```bash
    java -server -Xms512m -Xmx1024m -Dspring.config.additional-location=/home/<USER>/.config/sar/ -jar sar-pod-x.y.z.jar
    ``` 

 5. That's it !
 
    Your configuration file should have been processed.


## Build from source (database + Pod) 

 1. Installe project dependencies:
    * Install build tools (Make, GCC, Git)
      ```bash 
      sudo apt-get install build-essential
      ```
   * Install LibSodium (Unix only):
     https://download.libsodium.org/doc/installation/

   * Install Java SDK 11

   * Install Apache Maven 3

 2. Get the source code
    ```bash 
    git clone git@github.com:simm-sar/sar-pod.git
    cd sar-pod
    ```

 3. Run the compilation:
    ```bash
    cd sar-pod
    mvn install -DskipTests
    ```

 4. The final JAR file should have been created inside the directory: `<PROJECT_DIR>/sar-server/target/`  
